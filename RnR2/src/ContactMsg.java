
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class ContactMsg{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
        
    public ContactMsg()
  
       {
        frame = new JFrame("R&R Support_Επικοινωνία με Πελάτη");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Πίσω");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
       
        JLabel one = new JLabel("Όνομα Πελάτη: xxxx");
        one.setForeground(Color.white);
        
        JLabel two = new JLabel("Email Πελάτη: xxxx");
        two.setForeground(Color.white);
        
        JLabel three = new JLabel("Τηλέφωνο Πελάτη: xxxx");
        three.setForeground(Color.white);
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		           frame.setVisible(false);
        		       }
        		      });
        		 
        GridBagLayout layout = new GridBagLayout();
        //panel.setLayout(layout);
        JPanel leftPanel = new JPanel(layout);
        JPanel rightPanel = new JPanel(layout);
        panel = new JPanel(new GridLayout(1,1));
        panel.add(leftPanel, BorderLayout.EAST);
        panel.add(rightPanel, BorderLayout.WEST);
        leftPanel.setBackground(Color.BLACK);
        rightPanel.setBackground(Color.BLACK);

        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy =1;
        c.ipady = 40;
        leftPanel.add(label, c);
  
       c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        c.insets = new Insets(3, 3, 3, 3);
        c.weighty = 0.0;
        c.gridx = 1;
        c.gridy =1;
        c.weighty = 0.0;///0
        c.ipady = 0;
       c.gridwidth = 1;
       rightPanel.add(one, c);
  
        c.gridx = 1;
       c.gridy =2;
       c.gridwidth = 1;
       rightPanel.add(three, c);

       c.anchor = GridBagConstraints.PAGE_END;
       c.gridx = 1;
       c.gridy =3;
       c.gridwidth = 1;
       c.weighty = 0.0;///0
       c.ipady = 0;
       rightPanel.add(two, c);
         
       c.gridx = 1;
       c.gridy =4;
       c.gridwidth = 1;
       rightPanel.add(button, c);
        
        frame.add(panel);
        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
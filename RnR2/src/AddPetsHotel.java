import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class AddPetsHotel{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Pets animal = new Pets() ;
        
    public AddPetsHotel()
  
       {
        frame = new JFrame("R&R Support_Προσθήκη Προσφοράς για Κατοικίδια");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Προσθήκη");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
      
      JCheckBox food = new JCheckBox("Προσφορά Τροφής");
      food.setBackground(Color.BLACK);
      food.setForeground(Color.WHITE);
      food.setMnemonic(KeyEvent.VK_C);
      food.setSelected(false);
      
      JCheckBox walk = new JCheckBox("Προσφορά Βόλτας");
      walk.setBackground(Color.BLACK);
      walk.setForeground(Color.WHITE);
      walk.setMnemonic(KeyEvent.VK_D);
      walk.setSelected(false);
      
      JCheckBox train = new JCheckBox("Προσφορά Εκπαίδευσης");
      train.setBackground(Color.BLACK);
      train.setForeground(Color.WHITE);
      train.setMnemonic(KeyEvent.VK_E);
      train.setSelected(false);
      
      
       JLabel foodCost = new JLabel("Κόστος προσφοράς τροφής ανά ημέρα:");
        JTextField foodCostTF = new JTextField(20);
        foodCostTF.setEditable(false);
        foodCost.setLabelFor(foodCostTF);
        foodCost.setForeground(Color.white);
        
         JLabel walkCost = new JLabel("Κόστος προσφοράς βόλτας ανά ημέρα:");
        JTextField walkCostTF = new JTextField(20);
        walkCostTF.setEditable(false);
        walkCost.setLabelFor(walkCostTF);
        walkCost.setForeground(Color.white);
        
         JLabel trainCost = new JLabel("Κόστος προσφοράς εκπαίδευσης ανά ημέρα:");
        JTextField trainCostTF = new JTextField(20);
        trainCostTF.setEditable(false);
        trainCost.setLabelFor(trainCostTF);
        trainCost.setForeground(Color.white);
        
        food.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(food.isSelected()==true) {
					foodCostTF.setEditable(true);
				}
			}
		      });
        
        walk.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(walk.isSelected()==true) {
					walkCostTF.setEditable(true);
				}
			}
		      });
        
        train.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(train.isSelected()==true) {
					trainCostTF.setEditable(true);
				}
			}
		      });
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		    	   frame.setVisible(false);
        		    	   if(food.isSelected()==true) {
        		    		   animal.setHotelFood(true,Double.parseDouble(foodCostTF.getText()));
        		    		}
        		    	   if(walk.isSelected()==true) {    
        		    		   animal.setHotelWalk(true, Double.parseDouble(walkCostTF.getText()));
        		    	   }
        		    	   if(train.isSelected()==true) {
            		    	   animal.setHotelTraining(true, Double.parseDouble(trainCostTF.getText()));
        		    	   }
        		    	   
        		           
        		       }
        		      });
        		 
        		 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(food)
              		           .addComponent(walk)
              		           .addComponent(train)
              		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(foodCost)
                      		           .addComponent(walkCost)
                      		           .addComponent(trainCost))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    		           .addComponent(foodCostTF)
                    		           .addComponent(walkCostTF)
                    		           .addComponent(trainCostTF)) 
              		         );
              		           
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(food)
              		           .addComponent(foodCost)
              		           .addComponent(foodCostTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(walk)
              		    		  .addComponent(walkCost)
              		    		  .addComponent(walkCostTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(train)
              		    		  .addComponent(trainCost)
              		    		.addComponent(trainCostTF))
              		    
      		    		  .addComponent(button)
             		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
      public Pets returnPet() {
    	  return animal;
      }
}
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.awt.image.*;
public class Menu
 {
 
    public static void main(String s[]) {
       
       Log log=new Log(); 
       Sign sign=new Sign(); 
       HotelSign ad=new HotelSign(); 

        JFrame frame = new JFrame("R&R Support");
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        ImageIcon img1 = new ImageIcon("..\\images\\logo.png");
        JPanel mpanel = new JPanel();
        Color grey= new Color(20,20,20);
		mpanel.setBackground(Color.BLACK);
        frame.setIconImage(img1.getImage());
        
        JLabel label =  new JLabel(img);
 
        JButton login = new JButton("LOG IN");
        login.setBackground(grey);
        login.setForeground(Color.WHITE);
    
       JButton hotel = new JButton("Προσθέστε το κατάλυμά σας");
       hotel.setBackground(grey);
       hotel.setForeground(Color.WHITE);
    
  
        JButton sup = new JButton("SIGN UP");
        sup.setBackground(grey);
        sup.setForeground(Color.WHITE);
      
         GridBagLayout layout = new GridBagLayout();
         //panel.setLayout(layout);
         JPanel leftPanel = new JPanel(layout);
         JPanel rightPanel = new JPanel(layout);
         mpanel = new JPanel(new GridLayout(1,1));
         mpanel.add(leftPanel, BorderLayout.EAST);
         mpanel.add(rightPanel, BorderLayout.WEST);
         leftPanel.setBackground(Color.BLACK);
         rightPanel.setBackground(Color.BLACK);

         
         GridBagConstraints c = new GridBagConstraints();
         c.gridx = 0;
         c.gridy =1;
         c.ipady = 40;
         leftPanel.add(label, c);
   
        c.fill = GridBagConstraints.HORIZONTAL;
         c.weightx = 1.0;
         c.insets = new Insets(3, 3, 3, 3);
         c.weighty = 0.0;
         c.gridx = 1;
         c.gridy =1;
        c.gridwidth = 1;
        rightPanel.add(sup, c);
   
         c.gridx = 2;
        c.gridy =1;
        c.gridwidth = 1;
        rightPanel.add(login, c);

        c.anchor = GridBagConstraints.PAGE_END;
        c.gridx = 1;
        c.gridy =2;
        c.gridwidth = 2;
        c.weighty = 0.0;///0
        c.ipady = 0;
        rightPanel.add(hotel, c);
        
        JFrame fr1=log.getFrame();
        fr1.setVisible(false);
        JFrame fr2=sign.getFrame();
        fr2.setVisible(false);
        JFrame fr3=ad.getFrame();
        fr3.setVisible(false);
        
        login.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent e){
         fr1.setVisible(true);
         frame.setVisible(false);
       }
      });
         
       
       sup.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent e){
         fr2.setVisible(true);
         frame.setVisible(false);
       }
      });
    
       hotel.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
             fr3.setVisible(true);
             frame.setVisible(false);
           }
          });
   
        frame.setUndecorated(true);
        frame.add(mpanel);
        frame.setSize(700,400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       // frame.pack();   
        frame.setVisible(true);
        
       
 
    }
 
}
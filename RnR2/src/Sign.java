import java.sql.*;
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class Sign{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  public String name,sname,email,password;
    public Sign()
  
       {
        frame = new JFrame("R&R Support_Sign Up");  
        Color grey= new Color(50,50,50);
        panel.setBackground(grey); 
        frame.setIconImage(img.getImage());
        JButton button = new JButton();
        button.setText("OK");
        button.setBackground(Color.BLACK);
       button.setForeground(Color.WHITE);
        

       JLabel lblName = new JLabel("Όνομα:");
        JTextField tfName = new JTextField(20);
        lblName.setLabelFor(tfName);
        lblName.setForeground(Color.white);
        
         JLabel lblSName = new JLabel("Επώνυμο:");
        JTextField tfSName = new JTextField(20);
        lblSName.setLabelFor(tfSName);
        lblSName.setForeground(Color.white);
        
         JLabel lblMail = new JLabel("e-mail:");
        JTextField tfMail = new JTextField(20);
        lblMail.setLabelFor(tfMail);
        lblMail.setForeground(Color.white);
        
        
        JLabel lblPassword = new JLabel("Password:");
        final JPasswordField pfPassword = new JPasswordField(20);
        lblPassword.setLabelFor(pfPassword);
        lblPassword.setForeground(Color.white);
          
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
        		   layout.createSequentialGroup()
        		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		           .addComponent(lblName)
        		           .addComponent(lblSName)
        		           .addComponent(lblMail)
        		           .addComponent(lblPassword)
        		           .addComponent(button))
        		           
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           		           .addComponent(tfName)
           		           .addComponent(tfSName)
           		           .addComponent(tfMail)
        		           .addComponent(pfPassword))
        		     
        		);
        		layout.setVerticalGroup(
        		   layout.createSequentialGroup()
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		           .addComponent(lblName)
        		           .addComponent(tfName))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblSName)
           		           .addComponent(tfSName))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblMail)
           		           .addComponent(tfMail))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblPassword)
           		           .addComponent(pfPassword))
        		      .addComponent(button)
        		      
        		);
        		
        		 button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		    	   name = tfName.getText();
            		       sname = tfSName.getText();
            		       email= tfMail.getText();
            		       password = String.valueOf(pfPassword.getPassword());
            		       //ID apo th bash
            		       Customer cust=new Customer(name,sname,email,password,0);
	            		   try
	               		    {
	               		      // create a mysql database connection     		     
	               		      Class.forName("com.mysql.jdbc.Driver");
	               		      Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/rnr","root","");
	               		      // the mysql insert statement
	               		      String query = " INSERT INTO `clients` (`customer_id`, `Name`, `Surname`, `Email`, `Password`) VALUES (NULL, ?, ?, ?, ?);";	               		      
	               		      // create the mysql insert preparedstatement
	               		      PreparedStatement preparedStmt = conn.prepareStatement(query);
	               		      //preparedStmt.setString (1, "NULL");
	               		      preparedStmt.setString (1, name);
	               		      preparedStmt.setString  (2, sname);
	               		      preparedStmt.setString(3, email);
	               		      preparedStmt.setString    (4, password);	
	               		      // execute the preparedstatement
	               		      preparedStmt.execute();	               		      
	               		      conn.close();	               		   
	               		    }
	               		    catch (Exception ee)
	               		    {
	               		      System.err.println("Got an exception!");
	               		      System.err.println(ee.getMessage());
	               		    }
            		       
            		       FirstPage menu=new FirstPage();
        		    	   JFrame fr1=menu.getFrame();
        		           fr1.setVisible(true);
        		       }
        		      });
   			
        frame.add(panel);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);         
        frame.setVisible(false);

    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
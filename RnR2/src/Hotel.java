import java.util.ArrayList;

public class Hotel {
	String address;
	String type;
	int stars;
	int rating;
	int phone;
	String email;
	String name;
	int availableRooms;
	String area;
	int bank;
	int numOneroom, numTworoom, numThreeroom, numFourroom, numFiveroom;
	double numOneroomPrice, numTworoomPrice, numThreeroomPrice, numFourroomPrice, numFiveroomPrice;
	boolean wifi, breakfast, parking;
	ArrayList<Activity> activities = new ArrayList<Activity>();
	ArrayList<Transportation> transport = new ArrayList<Transportation>();
	ArrayList<Pets> animals = new ArrayList<Pets>();
	ArrayList<Kids> kids = new ArrayList<Kids>();
	
	public Hotel(String address, String type, int stars, int rating, int phone, String email, String name, int availableRooms, String area, int bank, int numOneroom, int numTworoom, int numThreeroom, int numFourroom, int numFiveroom, boolean wifi, boolean breakfast, boolean parking){
		this.address=address;
		this.type=type;
		this.stars=stars;
		this.phone=phone;
		this.rating=rating;
		this.email=email;
		this.name=name;
		this.availableRooms=availableRooms;
		this.area=area;
		this.bank=bank;
		this.numOneroom=numOneroom;
		this.numTworoom=numTworoom;
		this.numThreeroom=numThreeroom;
		this.numFourroom=numFourroom;
		this.numFiveroom=numFiveroom;
		this.wifi = wifi;
		this.breakfast = breakfast;
		this.parking = parking;
	}
	
	public Hotel() {
		// TODO Auto-generated constructor stub
	}

	public boolean checkAvailableOffers(Activity activ, Transportation transp, Pets anim, Kids kid) {
		
		return true;
	}
	
	public void reservRoom() {
		
	}
	
	public boolean checkAvailableRoom() {
		return true;
	}
	
//	public ArrayList<Hotel> checkAndReturnHotels(){}

	public void addKid(Kids child) {
		kids.add(child);
	}
	
	public void addTransportation(Transportation transp) {
		transport.add(transp);
	}
	
	public void addPet(Pets animal) {
		animals.add(animal);
	}
	
	public void addActivity(Activity activ) {
		activities.add(activ);
	}

	public void setOne(int one) {
		this.numOneroom=one;
	}
	
	public void setTwo(int two) {
		this.numTworoom=two;
	}

	public void setThree(int three) {
		this.numThreeroom=three;
	}
	
	public void setFour(int four) {
		this.numFourroom=four;
	}
	
	public void setFive(int five) {
		this.numFiveroom=five;
	}
}


import java.util.Comparator;

public class compareHotelStarF implements Comparator<HelpSortHotel>{
	public int compare(HelpSortHotel a, HelpSortHotel b) {
		return b.stars - a.stars;
	}
}

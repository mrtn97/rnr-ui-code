import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
public class HotelSign{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  public String name,sname,email,password;
      
    public HotelSign()
  
       {
        frame = new JFrame("R&R Support_Δημιουργία λογαριασμού");  
        Color grey= new Color(50,50,50);
        panel.setBackground(grey);
        frame.setIconImage(img.getImage());
        JButton button = new JButton();
        button.setText("Συνέχεια");
        button.setBackground(Color.BLACK);
       button.setForeground(Color.WHITE);
        

       JLabel lblName = new JLabel("Όνομα:");
        JTextField tfName = new JTextField(20);
        lblName.setLabelFor(tfName);
        lblName.setForeground(Color.white);
        
         JLabel lblSName = new JLabel("Επώνυμο:");
        JTextField tfSName = new JTextField(20);
        lblSName.setLabelFor(tfSName);
        lblSName.setForeground(Color.white);
        
         JLabel lblMail = new JLabel("e-mail:");
        JTextField tfMail = new JTextField(20);
        lblMail.setLabelFor(tfMail);
        lblMail.setForeground(Color.white);
        
        
        JLabel lblPassword = new JLabel("Password:");
        final JPasswordField pfPassword = new JPasswordField(20);
        lblPassword.setLabelFor(pfPassword);
        lblPassword.setForeground(Color.white);
          
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
        		   layout.createSequentialGroup()
        		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		           .addComponent(lblName)
        		           .addComponent(lblSName)
        		           .addComponent(lblMail)
        		           .addComponent(lblPassword)
        		           .addComponent(button))
        		           
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           		           .addComponent(tfName)
           		           .addComponent(tfSName)
           		           .addComponent(tfMail)
        		           .addComponent(pfPassword))
        		     
        		);
        		layout.setVerticalGroup(
        		   layout.createSequentialGroup()
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		           .addComponent(lblName)
        		           .addComponent(tfName))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblSName)
           		           .addComponent(tfSName))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblMail)
           		           .addComponent(tfMail))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(lblPassword)
           		           .addComponent(pfPassword))
        		      .addComponent(button)
        		      
        		);
        		
        		 button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		    	   name = tfName.getText();
            		       sname = tfSName.getText();
            		       email= tfMail.getText();
            		       password = String.valueOf(pfPassword.getPassword());
	            		   try
	               		    {
	               		      // create a mysql database connection     		     
	               		      Class.forName("com.mysql.jdbc.Driver");
	               		      Connection conn2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/rnr","root","");
	               		      // the mysql insert statement
	               		      String query2 = "INSERT INTO `admins` (`admin_id`, `Name`, `Surname`, `Email`, `Password`, `hotel_id`) VALUES (NULL, ?, ?, ?, ?, NULL);";	               		      
	               		      // create the mysql insert preparedstatement
	               		      PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
	               		      //preparedStmt.setString (1, "NULL");
	               		      preparedStmt2.setString (1, name);
	               		      preparedStmt2.setString  (2, sname);
	               		      preparedStmt2.setString(3, email);
	               		      preparedStmt2.setString    (4, password);	
	               		      // execute the preparedstatement
	               		      preparedStmt2.execute();	               		      
	               		      conn2.close();	               		   
	               		    }
	               		    catch (Exception ee)
	               		    {
	               		      System.err.println("Got an exception!");
	               		      System.err.println(ee.getMessage());
	               		    }
            		       
            		       
        		    	   Add menu=new Add();
        		    	   JFrame fr1=menu.getFrame();
        		           fr1.setVisible(true);
        		           frame.setVisible(false);
        		       }
        		      });
        
        frame.add(panel);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class AddKidsHotel{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Kids child = new Kids();
        
    public AddKidsHotel()
  
       {
        frame = new JFrame("R&R Support_Προσθήκη Προσφοράς για Παιδιά");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Προσθήκη");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
      
      JCheckBox babysit = new JCheckBox("Babysitting");
      babysit.setBackground(Color.BLACK);
      babysit.setForeground(Color.WHITE);
      babysit.setMnemonic(KeyEvent.VK_C);
      babysit.setSelected(false);
      
      JCheckBox play = new JCheckBox("Παιδότοπος");
      play.setBackground(Color.BLACK);
      play.setForeground(Color.WHITE);
      play.setMnemonic(KeyEvent.VK_D);
      play.setSelected(false);
      
      JCheckBox camp = new JCheckBox("Συνεργασία με κατασκήνωση");
      camp.setBackground(Color.BLACK);
      camp.setForeground(Color.WHITE);
      camp.setMnemonic(KeyEvent.VK_E);
      camp.setSelected(false);
      
      
       JLabel campName = new JLabel("Όνομα Κατασκήνωσης:");
        JTextField campNameTF = new JTextField(20);
        campNameTF.setEditable(false);
        campName.setLabelFor(campNameTF);
        campName.setForeground(Color.white);
        
         JLabel campLink = new JLabel("Ιστότοπος Κατασκήνωσης:");
        JTextField campLinkTF = new JTextField(20);
        campLinkTF.setEditable(false);
        campLink.setLabelFor(campLinkTF);
        campLink.setForeground(Color.white);
        
         JLabel playTime = new JLabel("Ώρες διαθεσιμότητας παιδότοπου:");
        JTextField playTimeTF = new JTextField(20);
        playTimeTF.setEditable(false);
        playTime.setLabelFor(playTimeTF);
        playTime.setForeground(Color.white);
        
        JLabel playNum = new JLabel("Τιμές παιδότοπου:");
        JTextField playNumTF = new JTextField(20);
        playNumTF.setEditable(false);
        playNum.setLabelFor(playNumTF);
        playNum.setForeground(Color.white);
              
        JLabel babysitNum = new JLabel("Αριθμός διαθέσιμων babysitters:");
        JTextField babysitNumTF = new JTextField(20);
        babysitNumTF.setEditable(false);
        babysitNum.setLabelFor(babysitNumTF);
        babysitNum.setForeground(Color.white);
          
        JLabel babysitTime = new JLabel("Χρέωση babysitting:");
        JTextField babysitTimeTF = new JTextField(20);
        babysitTimeTF.setEditable(false);
        babysitTime.setLabelFor(babysitTimeTF);
        babysitTime.setForeground(Color.white);
        
        babysit.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(babysit.isSelected()==true) {
			        babysitNumTF.setEditable(true);
			        babysitTimeTF.setEditable(true);
				}
			}
		      });
        
        play.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(play.isSelected()==true) {
			        playTimeTF.setEditable(true);
			        playNumTF.setEditable(true);
				}
			}
		      });
        
        camp.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(camp.isSelected()==true) {
			        campNameTF.setEditable(true);
			        campLinkTF.setEditable(true);
				}
			}
		      });
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		           frame.setVisible(false);
        		    	   if(babysit.isSelected()==true) {
        		    		   child.setHotelBabysit(true,Double.parseDouble(babysitTimeTF.getText()),Integer.parseInt(babysitNumTF.getText()));
        		    		}
        		    	   if(camp.isSelected()==true) {    
        		    		   child.setHotelCamp(true, campLinkTF.getText(), campNameTF.getText());
        		    	   }
        		    	   if(play.isSelected()==true) {
            		    	   child.setHotelPlay(true, playTimeTF.getText(),Double.parseDouble(playNumTF.getText()));
        		    	   }
        		       }
        		      });
        		 
        		 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(babysit)
              		           .addComponent(play)
              		           .addComponent(camp)
              		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(babysitNum)
                      		           .addComponent(babysitTime)
                      		           .addComponent(playTime)
                      		         .addComponent(playNum)
                      		           .addComponent(campName)
                      		           .addComponent(campLink))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    		           .addComponent(babysitNumTF)
                    		           .addComponent(babysitTimeTF)
                    		           .addComponent(playTimeTF)
                    		           .addComponent(playNumTF)
                    		           .addComponent(campNameTF)
                    		           .addComponent(campLinkTF)) 
              		         );
              		           
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(babysit)
              		           .addComponent(babysitNum)
              		           .addComponent(babysitNumTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(babysitTime)
              		    		  .addComponent(babysitTimeTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(play)
              		    		  .addComponent(playTime)
              		    		  .addComponent(playTimeTF))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            		    		  .addComponent(playNum)
            		    		  .addComponent(playNumTF))
            		      
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(camp)
              		    		  .addComponent(campName)
              		    		.addComponent(campNameTF))
              		    
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(campLink)
              		    		  .addComponent(campLinkTF))

      		    		  .addComponent(button)
             		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
      public Kids returnChild() {
    	  return child;
      }
}
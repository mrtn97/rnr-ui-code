
import java.awt.BorderLayout;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.*;

import org.jdatepicker.DatePicker;
import org.jdatepicker.JDatePicker;

import java.awt.event.*;

public class Search{

	Searching areas = new Searching();

	JFrame frame;
  ImageIcon img =new ImageIcon("..\\images\\logo.png");
  JPanel panel = new JPanel();
        
    public Search()
  
       {    	
        frame = new JFrame("R&R Support_Αναζήτηση");  
        Color grey= new Color(50,50,50);
        JPanel mpanel = new JPanel();
		mpanel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        
        menu = new JMenu("Όνομα Χρήστη"); 
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);	
        
        menu = new JMenu("Get Help");
        menu.setMnemonic(KeyEvent.VK_B);
        menuBar.add(menu);

        JMenuItem faq = new JMenuItem("FAQ",
                KeyEvent.VK_C);
menu.add(faq);
faq.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
frame.setVisible(false);
Faq fa=new Faq(null);
fa.setOrigin(0);
JFrame fr1=fa.getFrame();
fr1.setVisible(true);
}
});

JMenuItem priv = new JMenuItem("Privacy Policy",
KeyEvent.VK_D);
menu.add(priv);
priv.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
   frame.setVisible(false);
   Privacy pol=new Privacy(null);
   pol.setOrigin(0);
   JFrame fr1=pol.getFrame();
   fr1.setVisible(true);
}
});

JMenuItem about = new JMenuItem("About",
   KeyEvent.VK_E);
menu.add(about);
about.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
   frame.setVisible(false);
   About ab=new About(null);
   ab.setOrigin(0);
   JFrame fr1=ab.getFrame();
   fr1.setVisible(true);
}
});
        
       
               
               JLabel label =  new JLabel(img);
       
     
      String[] item1 = {"Είδος δωματίου","Μονόκλινο","Δίκλινο","Τρίκλινο","Τετράκλινο","Άνω των 5 ατόμων"};
      JComboBox comb1= new JComboBox(item1);
      comb1.setForeground(Color.white);
      comb1.setBackground(Color.black);
       
      JLabel lblR = new JLabel("Περιοχή:");
        JTextField tfR = new JTextField(10);
        lblR.setLabelFor(tfR);
        lblR.setForeground(Color.white);
      
       JLabel lblC = new JLabel("Αριθμός ατόμων:");
        JTextField tfC = new JTextField(10);
        lblC.setLabelFor(tfC);
        lblC.setForeground(Color.white);
        
        
      String[] item2 = {"Sort by","Stars","Rating","Price"};
      JComboBox comb2= new JComboBox(item2);
      comb2.setForeground(Color.white);
      comb2.setBackground(Color.black);
      
      String[] item3 = {"Είδος Διάταξης","Αύξουσα","Φθίνουσα"};
      JComboBox comb3= new JComboBox(item3);
      comb3.setForeground(Color.white);
      comb3.setBackground(Color.black);   
      
       JCheckBox wifi = new JCheckBox("Wifi");
         wifi.setBackground(Color.BLACK);
        wifi.setForeground(Color.WHITE);
        wifi.setMnemonic(KeyEvent.VK_C);
        wifi.setSelected(false);
      
        JCheckBox breakf = new JCheckBox("Δωρεάν Πρωινό");
        breakf.setBackground(Color.BLACK);
        breakf.setForeground(Color.WHITE);
        breakf.setMnemonic(KeyEvent.VK_D);
       breakf.setSelected(false);
       
         JCheckBox park = new JCheckBox("Parking");
        park.setBackground(Color.BLACK);
         park.setForeground(Color.WHITE);
        park.setMnemonic(KeyEvent.VK_E);
      park.setSelected(false);
      
      JLabel date1 = new JLabel("Ημερομηνία άφιξης:");
      date1.setForeground(Color.white);
      
      DatePicker picker = new JDatePicker();
      picker.setTextEditable(true);
      picker.setShowYearButtons(true);

      
      JLabel date2 = new JLabel("Ημερομηνία αναχώρησης:");
      date2.setForeground(Color.white);
      
      DatePicker picker1 = new JDatePicker();
      picker1.setTextEditable(true);
      picker1.setShowYearButtons(true);
      
        JButton button = new JButton();
        button.setText("Επιβεβαίωση");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
       
       button.addActionListener(new ActionListener(){
	       public void actionPerformed(ActionEvent e){	    	   
	    	   int value = comb1.getSelectedIndex();
	    	   int sortvalue = comb2.getSelectedIndex();
	    	   int sorttype = comb3.getSelectedIndex();
	       	   areas.searchAndReturnHotel(tfR.getText(),value, breakf.isSelected(), wifi.isSelected(), park.isSelected(), Integer.parseInt(tfC.getText()),sortvalue, sorttype);
	    	   Results r = new Results(areas);
	    	   JFrame fr1 = r.getFrame();
	    	   frame.setVisible(false);
	           fr1.setVisible(true);
	       }
	      });
       
       
       
     
      JPanel leftPanel = new JPanel(new GridBagLayout());
      	

                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(lblR)
              		           .addComponent(lblC)
              		         .addComponent(date1)
              		           .addComponent((JComponent) picker)
              		         .addComponent(date2)
              		         .addComponent((JComponent) picker1)
              		           .addComponent(comb1)
              		           .addComponent(comb2)
              		           .addComponent(breakf)
              		         .addComponent(park)
         		    		  .addComponent(wifi)
            		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(tfR)
                      		           .addComponent(tfC)
                      		         .addComponent(comb3))
              		      );
                 		      
              		    layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(lblR)
              		           .addComponent(tfR))
              		                      		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    .addComponent(lblC)
       		           .addComponent(tfC))
              		    
              		  .addComponent(date1)
     		          .addComponent((JComponent) picker)
     		         .addComponent(date2)
     		         .addComponent((JComponent) picker1) 
              		    
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    .addComponent(comb1))
              		    
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    .addComponent(comb2)
              		  .addComponent(comb3))
              		    
              		    .addComponent(breakf)
              		    .addComponent(park)
              		    .addComponent(wifi)
              		    .addComponent(button)
              		    
              		              		      
              		);
                 
              		
                 mpanel = new JPanel(new GridLayout(1,1));
                 mpanel.add(leftPanel, BorderLayout.EAST);
                 mpanel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
                 
                 
               
        frame.add(mpanel);
        frame.setSize(800, 450);
        frame.setJMenuBar(menuBar);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
        frame.setVisible(false);
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
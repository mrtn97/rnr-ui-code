import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.MenuBar;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
import java.util.ArrayList;
public class Results{
	ArrayList<String> names= new ArrayList<String>();
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  int origin=1;
      Searching s1 = null;  
    public Results(Searching s1)
  
       {
    	this.s1=s1;
    	frame = new JFrame("R&R Support_Αποτελέσματα Αναζήτησης");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        
        menu = new JMenu("Όνομα Χρήστη"); 
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);	
        
        menu = new JMenu("Get Help");
        menu.setMnemonic(KeyEvent.VK_B);
        menuBar.add(menu);

        JMenuItem faq = new JMenuItem("FAQ",
                KeyEvent.VK_C);
menu.add(faq);
faq.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
frame.setVisible(false);
Faq fa=new Faq(null);
fa.setOrigin(0);
JFrame fr1=fa.getFrame();
fr1.setVisible(true);
}
});

JMenuItem priv = new JMenuItem("Privacy Policy",
KeyEvent.VK_D);
menu.add(priv);
priv.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
   frame.setVisible(false);
   Privacy pol=new Privacy(null);
   pol.setOrigin(0);
   JFrame fr1=pol.getFrame();
   fr1.setVisible(true);
}
});

JMenuItem about = new JMenuItem("About",
   KeyEvent.VK_E);
menu.add(about);
about.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
   frame.setVisible(false);
   About ab=new About(null);
   ab.setOrigin(0);
   JFrame fr1=ab.getFrame();
   fr1.setVisible(true);
}
});
        
        

       JLabel info = new JLabel("Hotels in "+s1.area+":"); 
       info.setForeground(Color.white);
       
       JButton button = new JButton();
       button.setText("Επιβεβαίωση");
       button.setBackground(grey);
      button.setForeground(Color.WHITE);
      
      button.addActionListener(new ActionListener(){
	       public void actionPerformed(ActionEvent e){
	           frame.setVisible(false);
	           Hotel_profile menu=new Hotel_profile();
	    	   JFrame fr1=menu.getFrame();
	           fr1.setVisible(true);
	       }
	      });
      
      JButton button2 = new JButton();
      button2.setText("Επιστροφή στην Αναζήτηση");
      button2.setBackground(grey);
     button2.setForeground(Color.WHITE);
     
     button2.addActionListener(new ActionListener(){
	       public void actionPerformed(ActionEvent e){
	           frame.setVisible(false);
	           Search menu2=new Search();
	    	   JFrame fr2=menu2.getFrame();
	           fr2.setVisible(true);
	       }
	      });
      
      String[] item = {"Επιλογή ξενοδοχείου"};
      JComboBox comb= new JComboBox(item);
      comb.setForeground(Color.white);
      comb.setBackground(Color.black);  
      
      
      names = s1.saveResults();
      
 	  for(String a : names){
 		  		comb.addItem(a);
 	  }
   
        
        JPanel leftPanel = new JPanel(new GridBagLayout());
        
        JPanel rightPanel = new JPanel();
        GroupLayout layout = new GroupLayout(rightPanel);
        rightPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
     		   layout.createSequentialGroup()
     		   			
     		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
     		        		   .addComponent(info)
     		        		  .addComponent(comb)
     		        		  .addComponent(button)
     		        		 .addComponent(button2))
 );
     		           
     		layout.setVerticalGroup(
     		   layout.createSequentialGroup()
     		      
     		  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      				 .addComponent(info)) 
     		 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
     				 .addComponent(comb)) 
     		  
	           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
   		    		  .addComponent(button)) 
	           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	        		   .addComponent(button2)) 
     		);
        
        panel = new JPanel(new GridLayout(1,1));
        panel.add(leftPanel, BorderLayout.EAST);
        panel.add(rightPanel, BorderLayout.WEST);
        leftPanel.setBackground(Color.BLACK);
        rightPanel.setBackground(Color.BLACK);

        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy =1;
        c.ipady = 40;
        leftPanel.add(label, c);
  


frame.add(panel);
frame.setSize(1500, 500);
frame.setLocationRelativeTo(null);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

frame.setVisible(false);
    
             
 
    }
 
    public void setOrigin(int or)
    {
        origin = or;
    }
    
      public JFrame getFrame()
    {
        return frame;
    }
}
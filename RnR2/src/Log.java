import java.sql.*;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
public class Log {
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png"); 
  JPanel panel = new JPanel();
  public String email,password;     
    public Log()
  
       {    	
        frame = new JFrame("R&R Support_Log In");  
        Color grey= new Color(50,50,50);
        panel.setBackground(grey);
        frame.setIconImage(img.getImage());
        
        JLabel hotel = new JLabel("<html>"+"<h3>"+"Log in-hotel admin"+"</h3>"+"</html>");
        hotel.setForeground(Color.white);
        
        JLabel hotelMail = new JLabel("e-mail:");
        JTextField hotelMailTF = new JTextField(20);
        hotelMail.setLabelFor(hotelMailTF);
        hotelMail.setForeground(Color.white);
        
        JLabel hotelPassword = new JLabel("Password:");
        final JPasswordField hotelPasswordPF = new JPasswordField(20);
        hotelPassword.setLabelFor(hotelPasswordPF);
        hotelPassword.setForeground(Color.white);
           
        JButton hotelButton = new JButton();
        hotelButton.setText("OK");
        hotelButton.setBackground(Color.BLACK);
        hotelButton.setForeground(Color.WHITE);
        
        JLabel user = new JLabel("<html>"+"<h3>"+"Log in-user"+"</h3>"+"</html>");
        user.setForeground(Color.white);
    
        JLabel userMail = new JLabel("e-mail:");
        JTextField userMailTF = new JTextField(20);
        userMail.setLabelFor(userMailTF);
        userMail.setForeground(Color.white);
        
        JLabel userPassword = new JLabel("Password:");
        final JPasswordField userPasswordPF = new JPasswordField(20);
        userPassword.setLabelFor(userPasswordPF);
        userPassword.setForeground(Color.white);
           
        JButton userButton = new JButton();
        userButton.setText("OK");
        userButton.setBackground(Color.BLACK);
        userButton.setForeground(Color.WHITE);
       
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
        		   layout.createSequentialGroup()
        		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		           .addComponent(hotel)
        		           .addComponent(hotelMail)
        		           .addComponent(hotelPassword)
        		           .addComponent(hotelButton)
        		           .addComponent(user)
           		           .addComponent(userMail)
           		           .addComponent(userPassword)
           		        .addComponent(userButton))
        		           
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		    		  .addComponent(hotelMailTF) 
        		    		  .addComponent(hotelPasswordPF)
           		           .addComponent(userMailTF)
           		           .addComponent(userPasswordPF))
        		     
        		);
        		layout.setVerticalGroup(
        		   layout.createSequentialGroup()
        		   .addComponent(hotel)
        		      
        		   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		           .addComponent(hotelMail)
        		           .addComponent(hotelMailTF))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(hotelPassword)
           		           .addComponent(hotelPasswordPF))
        		      
        		      .addComponent(hotelButton)
        		      .addComponent(user)        		      
   		           		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(userMail)
           		           .addComponent(userMailTF))
        		      
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           		           .addComponent(userPassword)
           		           .addComponent(userPasswordPF))
        		      
        		      .addComponent(userButton)       		      
        		);
        
//============= USER LOG IN BUTTON ====================
       userButton.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent e){
	       email= userMailTF.getText();
	       password = String.valueOf(userPasswordPF.getPassword());
	       try
  		    {
  		      // create a mysql database connection     		     
  		      Class.forName("com.mysql.jdbc.Driver");
  		      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/rnr","root","");
	        String sql = "select Email,Password from clients where Email='"+email+"'and Password='"+password+"'";
	        Statement st = con.createStatement();
	        ResultSet rs=st.executeQuery(sql);
	        System.out.println("flag1");
	       
	        int count = 0;
	        while(rs.next()){
	            count = count+1;
	        }
	        if (count==1){
	        	frame.setVisible(false);
	            JOptionPane.showMessageDialog(null,"User Found, Access Granted! \nWelcome "+email+"!");
	            FirstPage menu = new FirstPage();
	     	    JFrame fr1 = menu.getFrame();
	            fr1.setVisible(true);
	            
	        }
	        else if (count>1){
	            JOptionPane.showMessageDialog(null,"Duplicate User, Access Denied!");
	        } 
            else {
            JOptionPane.showMessageDialog(null, "Email or Password incorrect!");
             }
  		  }
  		    catch (Exception ee)
  		    {
  		      System.err.println("Got an exception!");
  		      System.err.println(ee.getMessage());
  		    }
	       
       }
      });
  
       
//============= HOTEL ADMIN LOG IN BUTTON ====================

        hotelButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
               email= hotelMailTF.getText();
     	       password = String.valueOf(hotelPasswordPF.getPassword()); 
     	       try
       		    {
       		      // create a mysql database connection     		     
       		      Class.forName("com.mysql.jdbc.Driver");
       		      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/rnr","root","");
     	        String sql = "select Email,Password from admins where Email='"+email+"'and Password='"+password+"'";
     	        Statement st = con.createStatement();
     	        ResultSet rs=st.executeQuery(sql);
     	        int count = 0;
     	        while(rs.next()){
     	            count = count+1;
     	        }
     	        if (count==1){
     	        	frame.setVisible(false);
     	            JOptionPane.showMessageDialog(null,"User Found, Access Granted! \nWelcome "+email+"!");
     	            HotelFirstPage menu=new HotelFirstPage(null);
             	    JFrame fr2=menu.getFrame();
                    fr2.setVisible(true);          
     	        }
     	        else if (count>1){
     	            JOptionPane.showMessageDialog(null,"Duplicate User, Access Denied!");
     	        } 
                 else {
                 JOptionPane.showMessageDialog(null, "Email or Password incorrect!");
                  }
       		  }
       		    catch (Exception ee)
       		    {
       		      System.err.println("Got an exception!");
       		      System.err.println(ee.getMessage());
       		    }
         	 
            }
           });
        
        frame.add(panel);
        frame.setSize(600, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
        frame.setVisible(false);

 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
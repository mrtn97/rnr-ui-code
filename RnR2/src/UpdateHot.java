
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class UpdateHot{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Kids child = new Kids();
  Transportation transp = new Transportation();
  Pets animal = new Pets();
  Activity activity = null;
  Hotel myhotel = new Hotel();
        
    public UpdateHot(Hotel myhotel)
  
       {
    	this.myhotel=myhotel;
        frame = new JFrame("R&R Support_Update Στοιχείων Ξενοδοχείου");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Επιβεβαίωση");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
       
       JButton basic = new JButton();
       basic.setText("Update Βασικών Στοιχείων");
       basic.setBackground(Color.BLACK);
       basic.setForeground(Color.WHITE);
       
       JButton trans = new JButton();
       trans.setText("Προσθήκη Προσφοράς για Μεταφορά");
       trans.setBackground(Color.BLACK);
       trans.setForeground(Color.WHITE);
        
      JButton activ = new JButton();
      activ.setText("Προσθήκη Προσφοράς για Δραστηριότητες");
      activ.setBackground(Color.BLACK);
      activ.setForeground(Color.WHITE);
      
      JButton pet = new JButton();
      pet.setText("Προσθήκη Προσφοράς για Κατοικίδια");
      pet.setBackground(Color.BLACK);
      pet.setForeground(Color.WHITE);
      
      JButton kid = new JButton();
      kid.setText("Προσθήκη Προσφοράς για Παιδιά");
      kid.setBackground(Color.BLACK);
      kid.setForeground(Color.WHITE);
        
        JLabel one = new JLabel("Διαθέσιμος αριθμός μονόκλινων:");
        JTextField oneTF = new JTextField(5);
        one.setLabelFor(oneTF);
        one.setForeground(Color.white);
        
        JLabel two = new JLabel("Διαθέσιμος αριθμός δίκλινων:");
        JTextField twoTF = new JTextField(5);
        two.setLabelFor(twoTF);
        two.setForeground(Color.white);
        
        JLabel three = new JLabel("Διαθέσιμος αριθμός τρίκλινων:");
        JTextField threeTF = new JTextField(5);
        three.setLabelFor(threeTF);
        three.setForeground(Color.white);
        
        JLabel four = new JLabel("Διαθέσιμος αριθμός τετράκλινων:");
        JTextField fourTF = new JTextField(5);
        four.setLabelFor(fourTF);
        four.setForeground(Color.white);
        
        JLabel five = new JLabel("Διαθέσιμος αριθμός άνω των 5 ατόμων:");
        JTextField fiveTF = new JTextField(5);
        five.setLabelFor(fiveTF);
        five.setForeground(Color.white);

        JLabel oneP = new JLabel("Τιμή μονόκλινων:");
        JTextField onePTF = new JTextField(5);
        oneP.setLabelFor(onePTF);
        oneP.setForeground(Color.white);
        
        JLabel twoP = new JLabel("Τιμή δίκλινων:");
        JTextField twoPTF = new JTextField(5);
        twoP.setLabelFor(twoPTF);
        twoP.setForeground(Color.white);
        
        JLabel threeP = new JLabel("Τιμή τρίκλινων:");
        JTextField threePTF = new JTextField(5);
        threeP.setLabelFor(threePTF);
        threeP.setForeground(Color.white);
        
        JLabel fourP = new JLabel("Τιμή τετράκλινων:");
        JTextField fourPTF = new JTextField(5);
        fourP.setLabelFor(threeP);
        fourP.setForeground(Color.white);
        
        JLabel fiveP = new JLabel("Τιμή άνω των 5:");
        JTextField fivePTF = new JTextField(5);
        fiveP.setLabelFor(fivePTF);
        fiveP.setForeground(Color.white);
        
        kid.addActionListener(new ActionListener(){
		       public void actionPerformed(ActionEvent e){
		    	   AddKidsHotel addKid =  new AddKidsHotel();
		    	   JFrame fr1=addKid.getFrame();
		           fr1.setVisible(true);
		           child = addKid.returnChild();
		       }
		      });
        
        trans.addActionListener(new ActionListener(){
		       public void actionPerformed(ActionEvent e){
		    	   AddTranspHotel addTransp =  new AddTranspHotel();
		    	   JFrame fr2=addTransp.getFrame();
		           fr2.setVisible(true);
		           transp = addTransp.returnTransp();
		       }
		      });
        
        pet.addActionListener(new ActionListener(){
		       public void actionPerformed(ActionEvent e){
		    	   AddPetsHotel addPet =  new AddPetsHotel();
		    	   JFrame fr4=addPet.getFrame();
		           fr4.setVisible(true);
		           animal = addPet.returnPet();
		       }
		      });
        
        activ.addActionListener(new ActionListener(){
		       public void actionPerformed(ActionEvent e){
		    	   AddActivityHotel addAct =  new AddActivityHotel();
		    	   JFrame fr5=addAct.getFrame();
		           fr5.setVisible(true);
		           activity = addAct.returnActiv();
		       }
		      });
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		           frame.setVisible(false);
        		         
        		           
        		    	   HotelFirstPage menu=new HotelFirstPage(myhotel);
        		    	   JFrame fr3=menu.getFrame();
        		           fr3.setVisible(true);
        		           if(transp!=null) {myhotel.addTransportation(transp);}
        		           if(child!=null) {myhotel.addKid(child);}
        		           if(animal!=null) {myhotel.addPet(animal);}
        		           if(activity!=null) {myhotel.addActivity(activity);}
        		       }
        		      });
        		 
        basic.addActionListener(new ActionListener(){
		       public void actionPerformed(ActionEvent e){
		           frame.setVisible(false);
		           UpdateBasicHot bas=new UpdateBasicHot(myhotel);
		    	   JFrame fr2=bas.getFrame();
		           fr2.setVisible(true);
		       }
		      });	 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		        		 .addComponent(one)
                  		           .addComponent(two)
                  		           .addComponent(three)
               		           .addComponent(four)
               		           .addComponent(five)
         		    		  .addComponent(oneP)
            		           .addComponent(twoP)
            		           .addComponent(threeP)
         		           .addComponent(fourP)
         		           .addComponent(fiveP)
              		           .addComponent(button))
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		        		 .addComponent(oneTF)
                  		           .addComponent(twoTF)
                  		           .addComponent(threeTF)
               		           .addComponent(fourTF)
               		           .addComponent(fiveTF)
                 		    		  .addComponent(onePTF)
                    		           .addComponent(twoPTF)
                    		           .addComponent(threePTF)
                 		           .addComponent(fourPTF)
                 		           .addComponent(fivePTF))              		           
              		           
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		    		.addComponent(basic)     
              		    		  .addComponent(trans)
                    		           .addComponent(activ)
                    		           .addComponent(pet)
                 		           .addComponent(kid))
      		      
              		   		);
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(one)
              		         .addComponent(oneTF)
              		           .addComponent(basic))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    .addComponent(two)
              		  .addComponent(twoTF)
       		           .addComponent(trans))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(three)
              		    		  .addComponent(threeTF)
              		    		  .addComponent(activ))              		      

              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(four)
              		    		  .addComponent(fourTF)
              		    		  .addComponent(pet))

              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(five)
              		    		  .addComponent(fiveTF)
              		    		  .addComponent(kid))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            		    		  .addComponent(oneP)
            		    		  .addComponent(onePTF))
                    		     
              		  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
          		    		  .addComponent(twoP)
          		    		  .addComponent(twoPTF))
                  		     
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(threeP)
      		    		  .addComponent(threePTF))
              		     
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(fourP)
      		    		  .addComponent(fourPTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                 		           .addComponent(fiveP)
                 		        .addComponent(fivePTF))
      		    		  .addComponent(button)

              		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
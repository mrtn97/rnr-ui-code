import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.*;

import org.jdatepicker.DatePicker;
import org.jdatepicker.JDatePicker;

import java.awt.event.*;
public class HotelAllReserv {
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
   Hotel myhotel = null;     
	public HotelAllReserv()
  
       {    	
        frame = new JFrame("R&R Support_Κρατήσεις");  
        Color grey= new Color(50,50,50);
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        ImageIcon img1 = new ImageIcon("..\\images\\logo.png");
        JPanel mpanel = new JPanel();
		mpanel.setBackground(Color.BLACK);
        frame.setIconImage(img1.getImage());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        
        menu = new JMenu("Όνομα Ξενοδοχείου");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);	
        
        menu = new JMenu("Get Help");
        menu.setMnemonic(KeyEvent.VK_B);
        menuBar.add(menu);

        JMenuItem faq = new JMenuItem("FAQ",
                                 KeyEvent.VK_C);
        menu.add(faq);
        faq.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
         	   Faq fa=new Faq(myhotel);
         	   JFrame fr1=fa.getFrame();
                fr1.setVisible(true);
            }
           });
        
        JMenuItem priv = new JMenuItem("Privacy Policy",
                KeyEvent.VK_D);
        	menu.add(priv);
        	priv.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   Privacy pol=new Privacy(myhotel);
             	   JFrame fr1=pol.getFrame();
                    fr1.setVisible(true);
                }
               });
        	
        	JMenuItem about = new JMenuItem("About",
                    KeyEvent.VK_E);
        	menu.add(about);
        	about.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   About ab=new About(myhotel);
             	   JFrame fr1=ab.getFrame();
                    fr1.setVisible(true);
                }
               });
        
        JLabel label =  new JLabel(img);
 
        JLabel chose = new JLabel("Επιλογή Ημερομηνίας:");
        chose.setForeground(Color.white);
        
        JButton button = new JButton("Επιβεβαίωση");
        button.setBackground(grey);
        button.setForeground(Color.WHITE);
        
        String[] item1 = {"Κρατήσεις","Κρατ1","Κρατ2","Κρατ3","Κρατ4","Κρατ5"};
        JComboBox comb1= new JComboBox(item1);
        comb1.setEnabled(false);
        comb1.setForeground(Color.white);
        comb1.setBackground(Color.black);
        
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	comb1.setEnabled(true);
            }
           });    
        
        JButton ok = new JButton("Πληροφορίες Κράτησης");
        ok.setBackground(grey);
        ok.setForeground(Color.WHITE);
        
        ok.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
         	   ReservHotel res=new ReservHotel();
         	   JFrame fr=res.getFrame();
                fr.setVisible(true);
            }
           });
        
        DatePicker datePick = new JDatePicker();
        datePick.setTextEditable(true);
        datePick.setShowYearButtons(true);
       
         GridBagLayout layout = new GridBagLayout();
         //panel.setLayout(layout);
         JPanel leftPanel = new JPanel(layout);
         JPanel rightPanel = new JPanel(layout);
         mpanel = new JPanel(new GridLayout(1,1));
         mpanel.add(leftPanel, BorderLayout.EAST);
         mpanel.add(rightPanel, BorderLayout.WEST);
         leftPanel.setBackground(Color.BLACK);
         rightPanel.setBackground(Color.BLACK);

         
         GridBagConstraints c = new GridBagConstraints();
         c.gridx = 0;
         c.gridy =1;
         c.ipady = 40;
         leftPanel.add(label, c);
   
        c.fill = GridBagConstraints.HORIZONTAL;
         c.insets = new Insets(3, 3, 3, 3);
         c.weighty = 0.0;
         c.gridx = 1;
         c.gridy =1;
        c.gridwidth = 1;
        c.ipady = 0;
        rightPanel.add(chose, c);
        
        c.gridx = 1;
        c.gridy =2;
        c.gridwidth = 1;
        rightPanel.add((JComponent) datePick, c);
   
         c.gridx = 1;
        c.gridy =3;
        c.gridwidth = 1;
        rightPanel.add(button, c);
        
        c.gridx = 1;
        c.gridy =4;
        c.gridwidth = 1;
        rightPanel.add(comb1, c);
        
        c.gridx = 1;
        c.gridy =5;
        c.gridwidth = 1;
        rightPanel.add(ok, c);

        frame.add(mpanel);
        frame.setSize(600, 400);
        frame.setJMenuBar(menuBar);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
        frame.setVisible(false);
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
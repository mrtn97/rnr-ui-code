import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class CustReserv {
	
	 JFrame frame;
	  ImageIcon img = new ImageIcon("..\\images\\logo.png"); 
	  JPanel panel = new JPanel();
	  //Hotel newHotel;
	        
	    public CustReserv() 

	    
	       {
	        frame = new JFrame("R&R Support_Κράτηση");  
	        Color grey= new Color(40,40,40);
	        panel.setBackground(Color.BLACK);
	        frame.setIconImage(img.getImage());
	       
	        ImageIcon img = new ImageIcon("..\\images\\Hotel.png");
	        JLabel label =  new JLabel(img);

	        JButton button = new JButton();
	        button.setText("Πληρωμή");
	        button.setBackground(grey);
	       button.setForeground(Color.WHITE);
	       
	       String[] item1 = {"Πλήθος υπόλοιπων επισκεπτών","1","2","3","4"};
	       JComboBox comb1= new JComboBox(item1);
	       comb1.setForeground(Color.white);
	       comb1.setBackground(Color.black);
	       
	       JLabel one = new JLabel("Επισκέπτης_1:");
	       JLabel name1 = new JLabel("Όνομα-Επώνυμο:");
	       JLabel mail1 = new JLabel("Email:");
	       JTextField name1TF = new JTextField(10);
	       JTextField mail1TF = new JTextField(10); 
	       name1.setLabelFor(name1TF);
	       mail1.setLabelFor(mail1TF);      
	       one.setForeground(Color.white);
	       name1.setForeground(Color.white);
           mail1.setForeground(Color.white);
	       one.setVisible(false);
           name1.setVisible(false);
	       name1TF.setVisible(false);
	       mail1.setVisible(false);
	       mail1TF.setVisible(false);
	        
		   JLabel two = new JLabel("Επισκέπτης_2:");
		   JLabel name2 = new JLabel("Όνομα-Επώνυμο:");
	       JLabel mail2 = new JLabel("Email:");
	       JTextField name2TF = new JTextField(10);
	       JTextField mail2TF = new JTextField(10); 
	       name2.setLabelFor(name2TF);
	       mail2.setLabelFor(mail2TF);      
	       two.setForeground(Color.white);
	       name2.setForeground(Color.white);
           mail2.setForeground(Color.white);
	       two.setVisible(false);
           name2.setVisible(false);
	       name2TF.setVisible(false);
	       mail2.setVisible(false);
	       mail2TF.setVisible(false);
		  
	       JLabel three = new JLabel("Επισκέπτης_3:");
		   JLabel name3 = new JLabel("Όνομα-Επώνυμο:");
	       JLabel mail3 = new JLabel("Email:");
	       JTextField name3TF = new JTextField(10);
	       JTextField mail3TF = new JTextField(10); 
	       name3.setLabelFor(name3TF);
	       mail3.setLabelFor(mail3TF);      
	       three.setForeground(Color.white);
	       name3.setForeground(Color.white);
           mail3.setForeground(Color.white);
	       three.setVisible(false);
           name3.setVisible(false);
	       name3TF.setVisible(false);
	       mail3.setVisible(false);
	       mail3TF.setVisible(false);
		        
		        
	       JLabel four = new JLabel("Επισκέπτης_4:");
	       JLabel name4 = new JLabel("Όνομα-Επώνυμο:");
	       JLabel mail4 = new JLabel("Email:");
	       JTextField name4TF = new JTextField(10);
	       JTextField mail4TF = new JTextField(10); 
	       name4.setLabelFor(name4TF);
	       mail4.setLabelFor(mail4TF);      
	       four.setForeground(Color.white);
	       name4.setForeground(Color.white);
           mail4.setForeground(Color.white);
	       four.setVisible(false);
           name4.setVisible(false);
	       name4TF.setVisible(false);
	       mail4.setVisible(false);
	       mail4TF.setVisible(false);  
			        
		    
	  
	        
	        button.addActionListener(new ActionListener(){
	        		       public void actionPerformed(ActionEvent e){
	        		           frame.setVisible(false);
	        		         //  CustReserv reserv=new CustReserv();   
	        		          // HotelFirstPage menu=new HotelFirstPage(newHotel);
	        		    	  // JFrame fr1=reserv.getFrame();
	        		          // fr1.setVisible(true);       		         		    
	        		           
	        		       }
	        		      });
	     
	        comb1.addItemListener(new ItemListener() {
                //@Override
                public void itemStateChanged(ItemEvent e) {
                    switch (e.getStateChange()) {
                        case ItemEvent.SELECTED:
                            Object value = comb1.getSelectedItem();
                            if ("1".equals(value)) {
                                one.setVisible(true);
                                name1.setVisible(true);
                                name1TF.setVisible(true);
                                mail1.setVisible(true);
                                mail1TF.setVisible(true);
                                } 
                            else if ("2".equals(value)) {
                            	one.setVisible(true);
                                name1.setVisible(true);
                                name1TF.setVisible(true);
                                mail1.setVisible(true);
                                mail1TF.setVisible(true);
                                two.setVisible(true);
                                name2.setVisible(true);
                                name2TF.setVisible(true);
                                mail2.setVisible(true);
                                mail2TF.setVisible(true);
                            } else if ("3".equals(value)) {
                            	one.setVisible(true);
                                name1.setVisible(true);
                                name1TF.setVisible(true);
                                mail1.setVisible(true);
                                mail1TF.setVisible(true);
                              
                                two.setVisible(true);
                                name2.setVisible(true);
                                name2TF.setVisible(true);
                                mail2.setVisible(true);
                                mail2TF.setVisible(true);one.setVisible(true);
                                
                                three.setVisible(true);
                                name3.setVisible(true);
                                name3TF.setVisible(true);
                                mail3.setVisible(true);
                                mail3TF.setVisible(true);
                              
                            }else if ("4".equals(value)) {
                             	one.setVisible(true);
                                name1.setVisible(true);
                                name1TF.setVisible(true);
                                mail1.setVisible(true);
                                mail1TF.setVisible(true);
                              
                                two.setVisible(true);
                                name2.setVisible(true);
                                name2TF.setVisible(true);
                                mail2.setVisible(true);
                                mail2TF.setVisible(true);one.setVisible(true);
                                
                                three.setVisible(true);
                                name3.setVisible(true);
                                name3TF.setVisible(true);
                                mail3.setVisible(true);
                                mail3TF.setVisible(true);
                                
                                four.setVisible(true);
                                name4.setVisible(true);
                                name4TF.setVisible(true);
                                mail4.setVisible(true);
                                mail4TF.setVisible(true);}
                            break;
                    }
                }
                });
	        
	        
	        		 
	                 JPanel leftPanel = new JPanel(new GridBagLayout());
	                 
	                 JPanel rightPanel = new JPanel();
	                 GroupLayout layout = new GroupLayout(rightPanel);
	                 rightPanel.setLayout(layout);
	                 layout.setAutoCreateGaps(true);
	                 layout.setAutoCreateContainerGaps(true);
	                 
	                 layout.setHorizontalGroup(
	              		   layout.createSequentialGroup()
	              		   
	              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	              		        		 .addComponent(comb1)
	              		          .addComponent(one)
	           		    		  .addComponent(two)
	           		    		 .addComponent(three)
	         		    		 .addComponent(four)
	              		      	   .addComponent(button)
	         		    		  .addComponent(name1)
	         		    		 .addComponent(name2)
	         		    		.addComponent(name3)
	         		    		.addComponent(name4)
	         		    		  //.addComponent(oneMail)
	         		    		  
	              		        		   )
	              		                        		           
		              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	                      		           .addComponent(name1TF)
	                      		         .addComponent(name2TF)
	                      		       .addComponent(name3TF)
	                      		     .addComponent(name4TF))
	              		        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	                      		           .addComponent(mail1)
	                      		         .addComponent(mail2)
	                      		        .addComponent(mail3)
	                      		        .addComponent(mail4))  
	              		                     		       
	              		     .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    		           .addComponent(mail1TF)
                    		           .addComponent(mail2TF)
                    		           .addComponent(mail3TF)
                    		           .addComponent(mail4TF)) 
	                		 );
	                 
	                 
	              		     
	              		layout.setVerticalGroup(             		   
	              			   //.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	              				layout.createSequentialGroup()
	              			   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	              				.addComponent(comb1))
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	      		    				   .addComponent(one))
	      		    	
	      		    				 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	  	      		    				.addComponent(name1)
	  	      		    				.addComponent(name1TF)
	  	      		    			    .addComponent(mail1)
	  	      		    				.addComponent(mail1TF)
	      		    						 )		   
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	      		    				   .addComponent(two))
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	  	      		    				.addComponent(name2)
	  	      		    				.addComponent(name2TF)
	  	      		    			    .addComponent(mail2)
	  	      		    				.addComponent(mail2TF)
	      		    						 )	
	              			 
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	      		    				   .addComponent(three))
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	  	      		    				.addComponent(name3)
	  	      		    				.addComponent(name3TF)
	  	      		    			    .addComponent(mail3)
	  	      		    				.addComponent(mail3TF)
	      		    						 )	
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	      		    				   .addComponent(four))
	              			 
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	  	      		    				.addComponent(name4)
	  	      		    				.addComponent(name4TF)
	  	      		    			    .addComponent(mail4)
	  	      		    				.addComponent(mail4TF)
	      		    						 )	
	              			 
	              			 .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	      		    				   .addComponent(button))

	              		              		      
	              		);
	                 
	                 panel = new JPanel(new GridLayout(1,1));
	                 panel.add(leftPanel, BorderLayout.EAST);
	                 panel.add(rightPanel, BorderLayout.WEST);
	                 leftPanel.setBackground(Color.BLACK);
	                 rightPanel.setBackground(Color.BLACK);

	                 
	                 GridBagConstraints c = new GridBagConstraints();
	                 c.gridx = 0;
	                 c.gridy =1;
	                 c.ipady = 40;
	                 leftPanel.add(label, c);
	           
	         
	        
	        frame.add(panel);
	        frame.setSize(900, 350);
	        frame.setLocationRelativeTo(null);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	         
	        frame.setVisible(false);
	             
	 
	    }
	 
	    
	      public JFrame getFrame()
	    {
	        return frame;
	    }

}

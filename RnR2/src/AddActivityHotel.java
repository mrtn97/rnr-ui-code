import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class AddActivityHotel{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Trips tri ;
  Sports spo ;
        
    public AddActivityHotel()
  
       {
        frame = new JFrame("R&R Support_Προσθήκη Προσφοράς Δραστηριοτήτων");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Προσθήκη");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
      
      JCheckBox sport = new JCheckBox("Αθλητισμός");
      sport.setBackground(Color.BLACK);
      sport.setForeground(Color.WHITE);
      sport.setMnemonic(KeyEvent.VK_C);
      sport.setSelected(false);
      
      JCheckBox trip = new JCheckBox("Εκδομή");
      trip.setBackground(Color.BLACK);
      trip.setForeground(Color.WHITE);
      trip.setMnemonic(KeyEvent.VK_D);
      trip.setSelected(false);
      
       JLabel tripDest = new JLabel("Προορισμός:");
        JTextField tripDestTF = new JTextField(20);
        tripDestTF.setEditable(false);
        tripDest.setLabelFor(tripDestTF);
        tripDest.setForeground(Color.white);
        
         JLabel tripDesc = new JLabel("Περιγραφή:");
        JTextField tripDescTF = new JTextField(20);
        tripDescTF.setEditable(false);
        tripDesc.setLabelFor(tripDescTF);
        tripDesc.setForeground(Color.white);
        
         JLabel tripCost = new JLabel("Κόστος εκδρομής:");
        JTextField tripCostTF = new JTextField(20);
        tripCostTF.setEditable(false);
        tripCost.setLabelFor(tripCostTF);
        tripCost.setForeground(Color.white);
        
        JLabel sportType = new JLabel("Τύπος αθλητικής δραστηριότητας:");
        JTextField sportTypeTF = new JTextField(20);
        sportTypeTF.setEditable(false);
        sportType.setLabelFor(sportTypeTF);
        sportType.setForeground(Color.white); 
        
         JLabel sportName = new JLabel("Όνομα εκπαιδευτή:");
        JTextField sportNameTF = new JTextField(20);
        sportNameTF.setEditable(false);
        sportName.setLabelFor(sportNameTF);
        sportName.setForeground(Color.white);
        
         JLabel sportCost = new JLabel("Κόστος αθλητικής δραστηριότητας:");
        JTextField sportCostTF = new JTextField(20);
        sportCostTF.setEditable(false);
        sportCost.setLabelFor(sportCostTF);
        sportCost.setForeground(Color.white);
        
        sport.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(sport.isSelected()==true) {
					sportCostTF.setEditable(true);
					sportNameTF.setEditable(true);
					sportTypeTF.setEditable(true);
				}
			}
		      });
        
        trip.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(trip.isSelected()==true) {
					tripDestTF.setEditable(true);
					tripDescTF.setEditable(true);
					tripCostTF.setEditable(true);
				}
			}
		      });
                
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		    	   frame.setVisible(false);
        		    	   if(sport.isSelected()==true) {
        		    		   spo.setHotelSport(true, sportNameTF.getText(), sportTypeTF.getText(), Double.parseDouble(sportCostTF.getText()));
        		    		}
        		    	   if(trip.isSelected()==true) {    
        		    		   tri.setHotelTrip(true, tripDestTF.getText(), tripDescTF.getText(),Double.parseDouble(tripCostTF.getText()));
        		    	   }
      		           
        		       }
        		      });
        		 
        		 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(sport)
              		           .addComponent(trip)
              		           .addComponent(button)
              		           )
              		              
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(sportType)
                      		           .addComponent(sportName)
                      		           .addComponent(sportCost)
                      		         .addComponent(tripDest)
                    		           .addComponent(tripDesc)
                    		           .addComponent(tripCost))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		        		.addComponent(sportTypeTF)
                   		           .addComponent(sportNameTF)
                   		           .addComponent(sportCostTF)
                   		         .addComponent(tripDestTF)
                 		           .addComponent(tripDescTF)
                 		           .addComponent(tripCostTF)) 
              		         );
              		           
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(sport)
              		           .addComponent(sportType)
              		           .addComponent(sportTypeTF))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
               		           .addComponent(sportName)
               		           .addComponent(sportNameTF))
              		    
              		  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(sportCost)
              		           .addComponent(sportCostTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(trip)
              		    		  .addComponent(tripDest)
              		    		  .addComponent(tripDestTF))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                		           .addComponent(tripDesc)
                		           .addComponent(tripDescTF))
               		    
               		  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
               		           .addComponent(tripCost)
               		           .addComponent(tripCostTF))
              		                    		    
      		    		  .addComponent(button)
             		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
      public Activity returnActiv() {
    	  if (tri!=null) {
        	  return tri;
    	  }
    	  else {
    		  return spo;
    	  }
      }
}
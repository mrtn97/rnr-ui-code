import java.util.Comparator;

public class compareHotelPriceA implements Comparator<HelpSortHotel>{
	public int compare(HelpSortHotel a, HelpSortHotel b) {
		int value = (int) (a.price - b.price);
		return value;
	}
}

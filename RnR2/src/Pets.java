import java.lang.*;
import java.time.LocalTime;
import java.util.*;

public class Pets {

	 private double price;
     private boolean food;
     private boolean walk;
     private boolean training;
     
     boolean Hotelfood = false;
     boolean Hotelwalk = false;
     boolean Hoteltraining = false;
     
     double priceFood;
     double priceWalk;
     double priceTraining;
     
     public void addFood(){
		this.price+=priceFood;
	    this.food=true;
}

public void addWalk() {
	this.price+=priceWalk;
	this.walk=true;
}

public void addTraining() {
	 this.price+=priceTraining;
	 this.training=true;
}

public double returnPrice() {
	return price;
}

public void setHotelFood(boolean food, double price) {
	this.Hotelfood=food;
	this.priceFood=price;
}

public void setHotelWalk(boolean walk, double price) {
	this.Hotelwalk=walk;
	this.priceWalk=price;
}

public void setHotelTraining(boolean training, double price) {
	this.Hoteltraining=training;
	this.priceTraining=price;
}

}

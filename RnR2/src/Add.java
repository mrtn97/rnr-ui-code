
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
public class Add{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Hotel newHotel = new Hotel();
        
    public Add()
  
       {
        frame = new JFrame("R&R Support_Φόρμα συμπλήρωσης στοιχείων ξενοδοχείου");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Επιβεβαίωση");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
       
      JCheckBox wifi = new JCheckBox("Wifi");
      wifi.setBackground(Color.BLACK);
      wifi.setForeground(Color.WHITE);
      wifi.setMnemonic(KeyEvent.VK_C);
      wifi.setSelected(false);
      
      JCheckBox breakf = new JCheckBox("Δωρεάν πρωινό");
      breakf.setBackground(Color.BLACK);
      breakf.setForeground(Color.WHITE);
      breakf.setMnemonic(KeyEvent.VK_D);
      breakf.setSelected(false);
      
      JCheckBox park = new JCheckBox("Parking");
      park.setBackground(Color.BLACK);
      park.setForeground(Color.WHITE);
      park.setMnemonic(KeyEvent.VK_E);
      park.setSelected(false);
      
      
       JLabel hotName = new JLabel("Όνομα:");
        JTextField hotNameTF = new JTextField(20);
        hotName.setLabelFor(hotNameTF);
        hotName.setForeground(Color.white);
        
         JLabel area = new JLabel("Περιοχή:");
        JTextField areaTF = new JTextField(20);
        area.setLabelFor(areaTF);
        area.setForeground(Color.white);
        
         JLabel address = new JLabel("Διεύθυνση:");
        JTextField addressTF = new JTextField(20);
        address.setLabelFor(addressTF);
        address.setForeground(Color.white);
              
        JLabel type = new JLabel("Τύπος:");
        JTextField typeTF = new JTextField(20);
        type.setLabelFor(typeTF);
        type.setForeground(Color.white);
          
        JLabel bank = new JLabel("Τραπεζικός Λογαριασμός:");
        JTextField bankTF = new JTextField(20);
        bank.setLabelFor(bankTF);
        bank.setForeground(Color.white);
        
        JLabel rooms = new JLabel("Συνολικός αριθμός δωματίων:");
        JTextField roomsTF = new JTextField(5);
        rooms.setLabelFor(roomsTF);
        rooms.setForeground(Color.white);
        
        JLabel one = new JLabel("Μονόκλινα:");
        JTextField oneTF = new JTextField(5);
        one.setLabelFor(oneTF);
        one.setForeground(Color.white);
        
        JLabel two = new JLabel("Δίκλινα:");
        JTextField twoTF = new JTextField(5);
        two.setLabelFor(twoTF);
        two.setForeground(Color.white);
        
        JLabel three = new JLabel("Τρίκλινα:");
        JTextField threeTF = new JTextField(5);
        three.setLabelFor(threeTF);
        three.setForeground(Color.white);
        
        JLabel four = new JLabel("Τετράκλινα:");
        JTextField fourTF = new JTextField(5);
        four.setLabelFor(fourTF);
        four.setForeground(Color.white);
        
        JLabel five = new JLabel("Άνω των 5 ατόμων:");
        JTextField fiveTF = new JTextField(5);
        five.setLabelFor(fiveTF);
        five.setForeground(Color.white);

        JLabel hotStars = new JLabel("Stars:");
        JTextField hotStarsTF = new JTextField(20);
        hotStars.setLabelFor(hotStarsTF);
        hotStars.setForeground(Color.white);
        
        JLabel hotNum = new JLabel("Αριθμός Τηλεφώνου:");
        JTextField hotNumTF = new JTextField(20);
        hotNum.setLabelFor(hotNumTF);
        hotNum.setForeground(Color.white);
        
        JLabel hotMail = new JLabel("Email:");
        JTextField hotMailTF = new JTextField(20);
        hotMail.setLabelFor(hotMailTF);
        hotMail.setForeground(Color.white);
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		           
        		           
        		           String hotelName = hotNameTF.getText();
        		           String area = areaTF.getText();
        		           String address = addressTF.getText();
        		           String type = typeTF.getText();
        		           String email = hotMailTF.getText();
        		           int phone = Integer.parseInt(hotNumTF.getText());
        		           int stars = Integer.parseInt(hotStarsTF.getText());
        		           int bank = Integer.parseInt(bankTF.getText());
        		           int roomNum = Integer.parseInt(roomsTF.getText());
        		           int oneNum = Integer.parseInt(oneTF.getText());
        		           int twoNum = Integer.parseInt(twoTF.getText());
        		           int threeNum = Integer.parseInt(threeTF.getText());
        		           int fourNum = Integer.parseInt(fourTF.getText());
        		           int fiveNum = Integer.parseInt(fiveTF.getText());
        		           boolean wif = wifi.isSelected();
        		           boolean breakfast = breakf.isSelected();
        		           boolean parking = park.isSelected();
        		           newHotel =  new Hotel(address, type, stars, 0,phone, email, hotelName, roomNum, area, bank, oneNum, twoNum, threeNum, fourNum, fiveNum, wif, breakfast, parking);
        		           
        		           try
	               		    {
	               		      // create a mysql database connection     		     
	               		      Class.forName("com.mysql.jdbc.Driver");
	               		      Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/rnr","root","");
	               		      // the mysql insert statement
	               		      String query = "INSERT INTO `hotel` (`Stars`, `Rating`, `Tel`, `Email`, `Name`, `AvailableRooms`, `Area`, `BankAccount`, `HotelID`, `Address`, `Num1`, `Num2`, `Num3`, `Num4`, `Num5`, `Price1`, `Price2`, `Price3`, `Price4`, `Price5`, `Type`, `Wifi`, `Parking`, `Breakfast`) "
	               		      		+ "VALUES (?,'0',?,?,?,?,?,?,NULL,?,?,?,?,?,?,'0','0','0','0','0',?,?,?,?);";
	               		      //String query = " INSERT INTO `hotel` (`admin_id`, `Name`, `Surname`, `Email`, `Password`) VALUES (NULL, ?, ?, ?, ?);";	               		      
	               		      // create the mysql insert preparedstatement
	               		      PreparedStatement preparedStmt = conn.prepareStatement(query);
	               		      //preparedStmt.setString (1, "NULL");
	               		      preparedStmt.setInt (1, stars);
	               		      preparedStmt.setInt (2, phone);
	               		      preparedStmt.setString(3, email);
	               		      preparedStmt.setString (4, hotelName);
	               		      preparedStmt.setInt (5, roomNum);
	               		      preparedStmt.setString (6, area);
	               		      preparedStmt.setInt (7, bank);
	               		      preparedStmt.setString (8, address);
	               		      preparedStmt.setInt (9, oneNum);
	               		      preparedStmt.setInt (10, twoNum);
	               		      preparedStmt.setInt (11, threeNum);
	               		      preparedStmt.setInt (12, fourNum);
	               		      preparedStmt.setInt (13, fiveNum);
	               		      preparedStmt.setString (14, type);
	               		      preparedStmt.setBoolean (15, wif);
	               		      preparedStmt.setBoolean (16, breakfast);
	               		      preparedStmt.setBoolean (17, parking);
	               		      // execute the preparedstatement
	               		      preparedStmt.execute();	               		      
	               		      conn.close();	               		   
	               		    }
	               		    catch (Exception ee)
	               		    {
	               		      System.err.println("Got an exception!");
	               		      System.err.println(ee.getMessage());
	               		    }
        		    	   
        		           frame.setVisible(false);
        		           
        		           HotelFirstPage menu=new HotelFirstPage(newHotel);
        		    	   JFrame fr1=menu.getFrame();
        		           fr1.setVisible(true);
        		           
        		       }
        		      });
        		 
        		 
                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(hotName)
              		           .addComponent(area)
              		           .addComponent(address)
              		           .addComponent(type)
              		           .addComponent(hotMail)
              		         .addComponent(hotNum)
              		       .addComponent(hotStars)
              		     .addComponent(bank)
              		         .addComponent(rooms)
         		    		  .addComponent(one)
            		           .addComponent(two)
            		           .addComponent(three)
         		           .addComponent(four)
         		           .addComponent(five)
              		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(hotNameTF)
                      		           .addComponent(areaTF)
                      		           .addComponent(addressTF)
                      		           .addComponent(typeTF)
                      		         .addComponent(hotMailTF)
                      		         .addComponent(hotNumTF)
                      		       .addComponent(hotStarsTF)
                      		           .addComponent(bankTF)
                      		         .addComponent(roomsTF)
                 		    		  .addComponent(oneTF)
                    		           .addComponent(twoTF)
                    		           .addComponent(threeTF)
                 		           .addComponent(fourTF)
                 		           .addComponent(fiveTF))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                     		           .addComponent(wifi)
                     		           .addComponent(breakf)
                     		           .addComponent(park))
                		 		);
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(hotName)
              		           .addComponent(hotNameTF)
              		           .addComponent(wifi))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            		    		  .addComponent(area)
            		    		  .addComponent(areaTF)
            		    		  .addComponent(breakf))
              		  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
          		    		  .addComponent(address)
          		    		  .addComponent(addressTF)
          		    		  .addComponent(park))
              		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		    		  .addComponent(type)
        		    		  .addComponent(typeTF))    
              		
              		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(hotMail)
      		    		  .addComponent(hotMailTF))
              		
              		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(hotNum)
      		    		  .addComponent(hotNumTF))
              		
              		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(hotStars)
      		    		  .addComponent(hotStarsTF))
              		
              		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		    		  .addComponent(bank)
        		    		  .addComponent(bankTF))
              		  
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    .addComponent(rooms)
       		           .addComponent(roomsTF))      
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(one)
      		    		  .addComponent(oneTF))

              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(two)
      		    		  .addComponent(twoTF))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(three)
      		    		  .addComponent(threeTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(four)
      		    		  .addComponent(fourTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                 		           .addComponent(five)
                 		        .addComponent(fiveTF))
      		    		  .addComponent(button)

              		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
      
}
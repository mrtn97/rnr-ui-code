
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class HotelFirstPage{
  JFrame frame;
  ImageIcon img1 = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Hotel myhotel = null;  
    /**
     * 
     */
    public HotelFirstPage(Hotel myhotel)
  
       {
    	this.myhotel = myhotel;
    	JFrame frame = new JFrame("R&R Support_Menu Ξενοδοχείου");
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        Color grey= new Color(40,40,40);
		panel.setBackground(Color.BLACK);
        frame.setIconImage(img1.getImage());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        
        menu = new JMenu("Όνομα Ξενοδοχείου");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);	
        
        menu = new JMenu("Get Help");
        menu.setMnemonic(KeyEvent.VK_B);
        menuBar.add(menu);

        JMenuItem faq = new JMenuItem("FAQ",
                                 KeyEvent.VK_C);
        menu.add(faq);
        faq.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
         	   Faq fa=new Faq(myhotel);
         	   JFrame fr1=fa.getFrame();
                fr1.setVisible(true);
            }
           });
        
        JMenuItem priv = new JMenuItem("Privacy Policy",
                KeyEvent.VK_D);
        	menu.add(priv);
        	priv.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   Privacy pol=new Privacy(myhotel);
             	   JFrame fr1=pol.getFrame();
                    fr1.setVisible(true);
                }
               });
        	
        	JMenuItem about = new JMenuItem("About",
                    KeyEvent.VK_E);
        	menu.add(about);
        	about.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   About ab=new About(myhotel);
             	   JFrame fr1=ab.getFrame();
                    fr1.setVisible(true);
                }
               });
        	
        JLabel label =  new JLabel(img);
 
        JButton reserv = new JButton("Κρατήσεις");
        reserv.setBackground(grey);
        reserv.setForeground(Color.WHITE);
        
        reserv.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
                HotelAllReserv res=new HotelAllReserv();
         	   JFrame up2=res.getFrame();
                up2.setVisible(true);
            }
           });
    
       JButton upd = new JButton("update στοιχείων");
       upd.setBackground(grey);
       upd.setForeground(Color.WHITE);
       
       upd.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               frame.setVisible(false);
        	   UpdateHot update=new UpdateHot(myhotel);
        	   JFrame up1=update.getFrame();
               up1.setVisible(true);
           }
          });
          
         GridBagLayout layout = new GridBagLayout();
         panel.setLayout(layout);

         GridBagConstraints c = new GridBagConstraints();
         c.gridx = 0;
         c.gridy =1;
         c.ipady = 40;
         panel.add(label, c);
   
        c.fill = GridBagConstraints.HORIZONTAL;
         c.weightx = 1.0;
         c.insets = new Insets(3, 3, 3, 3);
         c.weighty = 0.0;
         c.gridx = 1;
         c.gridy =1;
        c.gridwidth = 1;
        panel.add(reserv, c);
   
         c.gridx = 2;
        c.gridy =1;
        c.gridwidth = 1;
        panel.add(upd, c);
  	
        frame.add(panel);
        frame.setJMenuBar(menuBar);
        frame.setSize(700, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(true);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
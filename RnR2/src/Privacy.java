
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class Privacy{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
   int origin = 1; 
   Hotel myhotel = null;
    public Privacy(Hotel myhotel)
  
       {
    	this.myhotel=myhotel;
        frame = new JFrame("R&R Support_Privacy");  
        Color grey= new Color(20,20,20);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
        JButton button = new JButton();
        button.setText("Πίσω");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);


       JLabel info = new JLabel("<html>"+"<h2>"+"Πολιτική Απορρήτου"+"</h2>"+"<br/>"+"Αυτή είναι η πολιτική απορρήτου μας."+"</html>"); 
       info.setForeground(Color.white);
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel pict =  new JLabel(img);
        
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
        		   layout.createSequentialGroup()
        		   			.addComponent(pict)
        		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		           .addComponent(info)
        		           .addComponent(button))        		     
        		);
        		layout.setVerticalGroup(
        		   layout.createSequentialGroup()
        		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		           .addComponent(pict)
        		           .addComponent(info))        		      
        		      .addComponent(button)
        		      
        		);
        		
        		button.addActionListener(new ActionListener(){
     		       public void actionPerformed(ActionEvent e){
     		           frame.setVisible(false);
     		           if (origin == 0) {
     		    	   FirstPage menu=new FirstPage();
     		    	   JFrame fr1=menu.getFrame();
     		           fr1.setVisible(true);}
     		           else {
     		        	   HotelFirstPage menu=new HotelFirstPage(myhotel);
         		    	   JFrame fr1=menu.getFrame();
         		           fr1.setVisible(true);   
     		           }
     		       }
     		      });
        
        frame.add(panel);
        frame.setSize(700, 400);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    public void setOrigin(int or)
    {
        origin = or;
    }
    
      public JFrame getFrame()
    {
        return frame;
    }
}
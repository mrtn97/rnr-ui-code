import java.util.Comparator;

public class compareHotelPriceF implements Comparator<HelpSortHotel>{
	public int compare(HelpSortHotel a, HelpSortHotel b) {
		int value = (int) (b.price - a.price);
		return value;
	}
}

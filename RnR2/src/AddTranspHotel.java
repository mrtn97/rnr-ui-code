import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class AddTranspHotel{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
  Transportation trans = new Transportation();
        
    public AddTranspHotel()
  
       {
        frame = new JFrame("R&R Support_Προσθήκη Προσφοράς για Μεταφορά");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Προσθήκη");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
      
      JCheckBox transAir = new JCheckBox("Μεταφορά από/πρός το αεροδρόμιο");
      transAir.setBackground(Color.BLACK);
      transAir.setForeground(Color.WHITE);
      transAir.setMnemonic(KeyEvent.VK_C);
      transAir.setSelected(false);
      
      JCheckBox transPort = new JCheckBox("Μεταφορά από/πρός το λιμάνι");
      transPort.setBackground(Color.BLACK);
      transPort.setForeground(Color.WHITE);
      transPort.setMnemonic(KeyEvent.VK_D);
      transPort.setSelected(false);
      
      JCheckBox sale = new JCheckBox("Έκπτωση στα αεροπορικά εισητήρια");
      sale.setBackground(Color.BLACK);
      sale.setForeground(Color.WHITE);
      sale.setMnemonic(KeyEvent.VK_E);
      sale.setSelected(false);
      
      
       JLabel airCost = new JLabel("Κόστος μεταφοράς για αεροδρόμιο:");
        JTextField airCostTF = new JTextField(20);
        airCostTF.setEditable(false);
        airCost.setLabelFor(airCostTF);
        airCost.setForeground(Color.white);
        
         JLabel portCost = new JLabel("Κόστος μεταφοράς για λιμάνι:");
        JTextField portCostTF = new JTextField(20);
        portCostTF.setEditable(false);
        portCost.setLabelFor(portCostTF);
        portCost.setForeground(Color.white);
        
         JLabel web = new JLabel("Ιστότοπος για προσφορές αεροπορικών εισητηρίων:");
        JTextField webTF = new JTextField(20);
        webTF.setEditable(false);
        web.setLabelFor(webTF);
        web.setForeground(Color.white);
        
        transAir.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(transAir.isSelected()==true) {
					airCostTF.setEditable(true);
				}
			}
		      });
        
        transPort.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(transPort.isSelected()==true) {
					portCostTF.setEditable(true);
				}
			}
		      });
        
        sale.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				if(sale.isSelected()==true) {
					webTF.setEditable(true);
				}
			}
		      });
        
        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		    	   frame.setVisible(false);
        		    	   if(transAir.isSelected()==true) {
        		    		   trans.setHotelAir(true,Double.parseDouble(airCostTF.getText()));
        		    		}
        		    	   if(transPort.isSelected()==true) {    
        		    		   trans.setHotelPort(true, Double.parseDouble(portCostTF.getText()));
        		    	   }
        		    	   if(sale.isSelected()==true) {
            		    	   trans.setHotelTicket(true, webTF.getText());
        		    	   }
        		    	   
        		           
        		       }
        		      });
        		 
        		 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(transAir)
              		           .addComponent(transPort)
              		           .addComponent(sale)
              		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(airCost)
                      		           .addComponent(portCost)
                      		           .addComponent(web))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    		           .addComponent(airCostTF)
                    		           .addComponent(portCostTF)
                    		           .addComponent(webTF)) 
              		         );
              		           
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(transAir)
              		           .addComponent(airCost)
              		           .addComponent(airCostTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(transPort)
              		    		  .addComponent(portCost)
              		    		  .addComponent(portCostTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(sale)
              		    		  .addComponent(web)
              		    		.addComponent(webTF))
              		    
      		    		  .addComponent(button)
             		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
      public Transportation returnTransp() {
    	  return trans;
      }
}
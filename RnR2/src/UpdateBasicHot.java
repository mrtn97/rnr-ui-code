
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class UpdateBasicHot{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
   Hotel myhotel = null;
    public UpdateBasicHot(Hotel myhotel)
  
       {
    	this.myhotel=myhotel;
        frame = new JFrame("R&R Support_Update Βασικών Στοιχείων Ξενοδοχείου");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Επιβεβαίωση");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
      
      JCheckBox wifi = new JCheckBox("Wifi");
      wifi.setBackground(Color.BLACK);
      wifi.setForeground(Color.WHITE);
      wifi.setMnemonic(KeyEvent.VK_C);
      wifi.setSelected(false);
      
      JCheckBox breakf = new JCheckBox("Δωρεάν Πρωινό");
      breakf.setBackground(Color.BLACK);
      breakf.setForeground(Color.WHITE);
      breakf.setMnemonic(KeyEvent.VK_D);
      breakf.setSelected(false);
      
      JCheckBox park = new JCheckBox("Parking");
      park.setBackground(Color.BLACK);
      park.setForeground(Color.WHITE);
      park.setMnemonic(KeyEvent.VK_E);
      park.setSelected(false);
      
      
       JLabel hotName = new JLabel("Όνομα:");
        JTextField hotNameTF = new JTextField(20);
        hotName.setLabelFor(hotNameTF);
        hotName.setForeground(Color.white);
        
         JLabel area = new JLabel("Περιοχή:");
        JTextField areaTF = new JTextField(20);
        area.setLabelFor(areaTF);
        area.setForeground(Color.white);
        
         JLabel address = new JLabel("Διεύθυνση:");
        JTextField addressTF = new JTextField(20);
        address.setLabelFor(addressTF);
        address.setForeground(Color.white);
              
        JLabel type = new JLabel("Τύπος:");
        JTextField typeTF = new JTextField(20);
        type.setLabelFor(typeTF);
        type.setForeground(Color.white);
          
        JLabel bank = new JLabel("Τραπεζικός Λογαριασμός:");
        JTextField bankTF = new JTextField(20);
        bank.setLabelFor(bankTF);
        bank.setForeground(Color.white);
        
        JLabel rooms = new JLabel("Συνολικός αριθμός δωματίων:");
        JTextField roomsTF = new JTextField(5);
        rooms.setLabelFor(roomsTF);
        rooms.setForeground(Color.white);
        
        JLabel one = new JLabel("Μονόκλινα:");
        JTextField oneTF = new JTextField(5);
        one.setLabelFor(oneTF);
        one.setForeground(Color.white);
        
        JLabel two = new JLabel("Δίκλινα:");
        JTextField twoTF = new JTextField(5);
        two.setLabelFor(twoTF);
        two.setForeground(Color.white);
        
        JLabel three = new JLabel("Τρίκλινα:");
        JTextField threeTF = new JTextField(5);
        three.setLabelFor(threeTF);
        three.setForeground(Color.white);
        
        JLabel four = new JLabel("Τετράκλινα:");
        JTextField fourTF = new JTextField(5);
        four.setLabelFor(fourTF);
        four.setForeground(Color.white);
        
        JLabel five = new JLabel("Άνω των 5 ατόμων:");
        JTextField fiveTF = new JTextField(5);
        five.setLabelFor(fiveTF);
        five.setForeground(Color.white);

        button.addActionListener(new ActionListener(){
        		       public void actionPerformed(ActionEvent e){
        		           frame.setVisible(false);
        		    	   HotelFirstPage menu=new HotelFirstPage(myhotel);
        		    	   JFrame fr1=menu.getFrame();
        		           fr1.setVisible(true);
        		       }
        		      });
        		 
        		 

                 JPanel leftPanel = new JPanel(new GridBagLayout());
                 
                 JPanel rightPanel = new JPanel();
                 GroupLayout layout = new GroupLayout(rightPanel);
                 rightPanel.setLayout(layout);
                 layout.setAutoCreateGaps(true);
                 layout.setAutoCreateContainerGaps(true);
                 
                 layout.setHorizontalGroup(
              		   layout.createSequentialGroup()
              		   			
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              		           .addComponent(hotName)
              		           .addComponent(area)
              		           .addComponent(address)
              		           .addComponent(type)
              		           .addComponent(bank)
              		         .addComponent(rooms)
         		    		  .addComponent(one)
            		           .addComponent(two)
            		           .addComponent(three)
         		           .addComponent(four)
         		           .addComponent(five)
              		           .addComponent(button)
              		           )
              		           
              		           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                      		           .addComponent(hotNameTF)
                      		           .addComponent(areaTF)
                      		           .addComponent(addressTF)
                      		           .addComponent(typeTF)
                      		           .addComponent(bankTF)
                      		         .addComponent(roomsTF)
                 		    		  .addComponent(oneTF)
                    		           .addComponent(twoTF)
                    		           .addComponent(threeTF)
                 		           .addComponent(fourTF)
                 		           .addComponent(fiveTF))              		           

              		         .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                     		           .addComponent(wifi)
                     		           .addComponent(breakf)
                     		           .addComponent(park))
              		         );
              		           
              		layout.setVerticalGroup(
              		   layout.createSequentialGroup()
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		           .addComponent(hotName)
              		           .addComponent(hotNameTF)
              		           .addComponent(wifi))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(area)
              		    		  .addComponent(areaTF)
              		    		  .addComponent(breakf))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(address)
              		    		  .addComponent(addressTF)
              		    		  .addComponent(park))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(type)
              		    		  .addComponent(typeTF))
              		    
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              		    		  .addComponent(bank)
              		    		  .addComponent(bankTF))
              		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      		    .addComponent(rooms)
               		           .addComponent(roomsTF))
              		    
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(one)
      		    		  .addComponent(oneTF))  
              		    
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(two)
      		    		  .addComponent(twoTF))
                      		      
              		    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            		    		  .addComponent(three)
            		    		  .addComponent(threeTF))
                    		        
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      		    		  .addComponent(four)
      		    		  .addComponent(fourTF))
              		      
              		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                 		           .addComponent(five)
                 		        .addComponent(fiveTF))
      		    		  .addComponent(button)

              		              		      
              		);
                 
                 panel = new JPanel(new GridLayout(1,1));
                 panel.add(leftPanel, BorderLayout.EAST);
                 panel.add(rightPanel, BorderLayout.WEST);
                 leftPanel.setBackground(Color.BLACK);
                 rightPanel.setBackground(Color.BLACK);

                 
                 GridBagConstraints c = new GridBagConstraints();
                 c.gridx = 0;
                 c.gridy =1;
                 c.ipady = 40;
                 leftPanel.add(label, c);
           
         
        
        frame.add(panel);
        frame.setSize(1500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
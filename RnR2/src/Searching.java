import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
public class Searching {
	String name;
	String address;
	String Price = "666";
	String hotelInfo;
	String area;
	String stars, rate;
	double prices=66.6;
	int breakf,wif,park;
	boolean breakfmatch, wifmatch,parkmatch,allmatch;
	ArrayList<String> HotelNames = new ArrayList<String>();
	ArrayList<HelpSortHotel> HotelSort = new ArrayList<HelpSortHotel>();
	int i=1;
	public void searchAndReturnHotel(String are, int noRooms, boolean breakfast, boolean wifi, boolean parking, int people, int sort, int sorttype) {
		this.area = are;
	try{  
		Class.forName("com.mysql.jdbc.Driver");  
		Connection con=DriverManager.getConnection(  
		"jdbc:mysql://localhost:3306/rnr","root","");  
		Statement stmt=con.createStatement();  
			
		ResultSet rs=stmt.executeQuery("select * from hotel where Area like '"+area+"' ");  
		while (rs.next())
	      {
			breakf = Integer.parseInt(rs.getString("Breakfast"));
			wif = Integer.parseInt(rs.getString("Wifi"));
			park = Integer.parseInt(rs.getString("Parking"));
			breakfmatch = (breakfast==true)&&(breakf==1);
			wifmatch = (wif==1)&&(wifi==true);
			parkmatch = (park==1)&&(parking==true);
			if(((breakfast==false)&&(wifi==false)&&(parking==false)) || ((parkmatch==true)&&(wifi==false)&&(breakfast==false)) || ((wifmatch==true)&&(breakfast==false)&&(parking==false)) || ((breakfmatch==true)&&(parking==false)&&(wifi==false)) || ((parkmatch==true)&&(wifmatch==true)&&(breakfmatch==true)) || ((parking==false)&&(wifmatch==true)&&(breakfmatch==true)) || ((parkmatch==true)&&(wifmatch==true)&&(breakfast==false)) || ((parkmatch==true)&&(breakfmatch==true)&&(wifi==false)))
							
			{
	       name = rs.getString("Name");
	       address = rs.getString("Address");
	       stars = rs.getString("Stars");
			rate = rs.getString("Rating");
	       if ((noRooms==1)||(people==1)) {
	    	   Price ="Tιμή μονόκλινου ";
	    	   prices = Double.parseDouble(rs.getString("Price1"));
	       }
	       else if ((noRooms==2)||(people==2)) {
	    	   Price = "Tιμή δίκλινου ";
	    	   prices = Double.parseDouble(rs.getString("Price2"));
	       }
	       else if ((noRooms==3)||(people==3)) {
	    	   Price = "Tιμή τρίκλινου ";
	    	   prices = Double.parseDouble(rs.getString("Price3"));
	       }
	       else if ((noRooms==4)||(people==4)) {
	    	   Price = "Tιμή τετράκλινου ";
	    	   prices = Double.parseDouble(rs.getString("Price4"));
	       }
	       else if ((noRooms==5)||(people>=5)) {
	    	   Price = "Tιμή πεντάκλινου ή άνω ";
	    	   prices = Double.parseDouble(rs.getString("Price5"));
	       }
	       HotelSort.add(new HelpSortHotel(Integer.parseInt(stars),Double.parseDouble(rate),prices,name,address,Price));
	       
	      }
			}
		stmt.close();
			
		}catch(Exception e){ System.out.println(e);}  
		if(sort!=0) {
			if (sort==1) {
				if (sorttype==1)
					Collections.sort(HotelSort, new compareHotelStarA());
				else if (sorttype==2)
					Collections.sort(HotelSort, new compareHotelStarF());
			}
			else if (sort==2) {
				if (sorttype==1)
					Collections.sort(HotelSort, new compareHotelRateA());
				else if (sorttype==2)
					Collections.sort(HotelSort, new compareHotelRateF());
			}
			else if (sort==3) {
				if (sorttype==1)
					Collections.sort(HotelSort, new compareHotelPriceA());
				else if (sorttype==2)
					Collections.sort(HotelSort, new compareHotelPriceF());
			}
			
		}
		
		for(int j =0 ; j<HotelSort.size(); j++) {
			hotelInfo = HotelSort.get(j).name + ", " + HotelSort.get(j).address + ", Stars:" + Integer.toString(HotelSort.get(j).stars) + ", Rating:" + Double.toString(HotelSort.get(j).rating) + ", "+ HotelSort.get(j).typeRoom +  Double.toString( HotelSort.get(j).price) + "$.";
			HotelNames.add(hotelInfo);
		}
		
	}
	
	public ArrayList<String> saveResults() {
		return HotelNames;
	}
	


	
}

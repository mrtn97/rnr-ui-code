
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.*;

import java.awt.event.*;
public class ResrvDet{
  JFrame frame;
  ImageIcon img = new ImageIcon("..\\images\\logo.png");  
  JPanel panel = new JPanel();
   Hotel myhotel = null; 
    public ResrvDet()
  
       {
        frame = new JFrame("R&R Support_Στοιχεία Κράτησης");  
        Color grey= new Color(40,40,40);
        panel.setBackground(Color.BLACK);
        frame.setIconImage(img.getImage());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        
        menu = new JMenu("Όνομα Ξενοδοχείου");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);	
        
        menu = new JMenu("Get Help");
        menu.setMnemonic(KeyEvent.VK_B);
        menuBar.add(menu);

        JMenuItem faq = new JMenuItem("FAQ",
                                 KeyEvent.VK_C);
        menu.add(faq);
        faq.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
         	   Faq fa=new Faq(myhotel);
         	   JFrame fr1=fa.getFrame();
                fr1.setVisible(true);
            }
           });
        
        JMenuItem priv = new JMenuItem("Privacy Policy",
                KeyEvent.VK_D);
        	menu.add(priv);
        	priv.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   Privacy pol=new Privacy(myhotel);
             	   JFrame fr1=pol.getFrame();
                    fr1.setVisible(true);
                }
               });
        	
        	JMenuItem about = new JMenuItem("About",
                    KeyEvent.VK_E);
        	menu.add(about);
        	about.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    frame.setVisible(false);
             	   About ab=new About(myhotel);
             	   JFrame fr1=ab.getFrame();
                    fr1.setVisible(true);
                }
               });
       
        ImageIcon img = new ImageIcon("..\\images\\logo.png");
        JLabel label =  new JLabel(img);

        JButton button = new JButton();
        button.setText("Πίσω");
        button.setBackground(grey);
       button.setForeground(Color.WHITE);
       
        JLabel one = new JLabel("Όνομα Πελάτη: xxxx");
        one.setForeground(Color.white);
        
        JLabel two = new JLabel("Email Πελάτη: xxxx");
        two.setForeground(Color.white);
        
        JLabel three = new JLabel("Είδος Πληρωμής: xxxx");
        three.setForeground(Color.white);
        
        JLabel four = new JLabel("Είδος Δωματίου: xxxx");
        four.setForeground(Color.white);
        
        JLabel five = new JLabel("Παροχές: xxxx");
        five.setForeground(Color.white);
        
        JLabel six = new JLabel("Στοιχεία υπόλοιπων πελατών: xxxx");
        six.setForeground(Color.white);
        
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
                ReservHotel b=new ReservHotel();
         	   JFrame fr1=b.getFrame();
                fr1.setVisible(true);
            }
           });
        		 
        GridBagLayout layout = new GridBagLayout();
        //panel.setLayout(layout);
        JPanel leftPanel = new JPanel(layout);
        JPanel rightPanel = new JPanel(layout);
        panel = new JPanel(new GridLayout(1,1));
        panel.add(leftPanel, BorderLayout.EAST);
        panel.add(rightPanel, BorderLayout.WEST);
        leftPanel.setBackground(Color.BLACK);
        rightPanel.setBackground(Color.BLACK);

        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy =1;
        c.ipady = 40;
        leftPanel.add(label, c);
  
       c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        c.insets = new Insets(3, 3, 3, 3);
        c.weighty = 0.0;
        c.gridx = 1;
        c.gridy =1;
        c.weighty = 0.0;///0
        c.ipady = 0;
       c.gridwidth = 1;
       rightPanel.add(one, c);
  
        c.gridx = 1;
       c.gridy =2;
       rightPanel.add(six, c);

       c.anchor = GridBagConstraints.PAGE_END;
       c.gridx = 1;
       c.gridy =3;
       c.gridwidth = 1;
       c.weighty = 0.0;///0
       c.ipady = 0;
       rightPanel.add(two, c);
       
       c.gridx = 1;
       c.gridy =4;
       c.gridwidth = 1;
       rightPanel.add(three, c);
       
       c.gridx = 1;
       c.gridy =5;
       c.gridwidth = 1;
       rightPanel.add(four, c);
       
       c.gridx = 1;
       c.gridy =6;
       c.gridwidth = 1;
       rightPanel.add(five, c);
         
       c.gridx = 1;
       c.gridy =7;
       c.gridwidth = 1;
       rightPanel.add(button, c);
        
       frame.add(panel);
       frame.setJMenuBar(menuBar);
       frame.setSize(700, 400);
       frame.setLocationRelativeTo(null);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
       frame.setVisible(false);
             
 
    }
 
    
      public JFrame getFrame()
    {
        return frame;
    }
}
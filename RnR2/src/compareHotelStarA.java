import java.util.Comparator;

public class compareHotelStarA implements Comparator<HelpSortHotel>{
	public int compare(HelpSortHotel a, HelpSortHotel b) {
		return a.stars - b.stars;
	}
}

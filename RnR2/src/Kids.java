import java.time.LocalTime;

public class Kids {
	int price;
	String dateOf;
	LocalTime timeOf;
	
	double HotelbabysitPrice;
	int HotelbabysittersNum;
	
	String HotelplayHours;
	double HotelplayPrice;
	
	String HotelCampLink;
	String HotelCampName;
	
	boolean Hotelbabysit=false;
	boolean Hotelplay=false;
	boolean Hotelcamp=false;

	
	boolean babysit=false;
	boolean play=false;
	boolean camp=false;

	public Kids() {		
		
	}
	
	public void askForBabysitting(String dat, LocalTime tim) {
		this.timeOf=tim;
		this.dateOf=dat;
		this.price+=HotelbabysitPrice;
		this.babysit=true;
	}
	
	public void addHappyPlace() {
		this.price+=HotelplayPrice;
		this.play=true;
	}
	
	public void addCamp() {
		this.camp=true;
	}
	
	public int returnPrice() {
		return price;
	}
	
	public void setHotelBabysit(boolean baby, double price, int num) {
		this.Hotelbabysit=baby;
		this.HotelbabysitPrice=price;
		this.HotelbabysittersNum=num;		
	}
	
	public void setHotelPlay(boolean play, String availableHours, double price) {
		this.Hotelplay=play;
		this.HotelplayHours=availableHours;
		this.HotelplayPrice=price;
	}
	
	public void setHotelCamp(boolean camp, String link, String name) {
		this.Hotelcamp=camp;
		this.HotelCampLink=link;
		this.HotelCampName=name;
	}
}

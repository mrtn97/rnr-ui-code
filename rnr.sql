-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2019 at 03:35 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rnr`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(4) NOT NULL,
  `Name` varchar(200) COLLATE utf8_bin NOT NULL,
  `Surname` varchar(200) COLLATE utf8_bin NOT NULL,
  `Email` varchar(200) COLLATE utf8_bin NOT NULL,
  `Password` varchar(200) COLLATE utf8_bin NOT NULL,
  `hotel_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `Name`, `Surname`, `Email`, `Password`, `hotel_id`) VALUES
(1, 'Γιώργος', 'Παναγόπουλος', 'gpan@gmail.com', '12345', 2),
(2, 'root', 'root', 'root', 'support', NULL),
(3, '', '', 'something', 'try', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `customer_id` int(11) NOT NULL,
  `Name` varchar(20) COLLATE utf8_bin NOT NULL,
  `Surname` varchar(20) COLLATE utf8_bin NOT NULL,
  `Email` varchar(20) COLLATE utf8_bin NOT NULL,
  `Password` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`customer_id`, `Name`, `Surname`, `Email`, `Password`) VALUES
(1, 'Γιώργος', 'Παπαδόπουλος', 'gpap@gmail.com', '12345\''),
(2, 'Μαριτίνα', 'Αφεντάκη', 'maraf@yahoo.gr', '123456'),
(3, 'Παντελής', 'Χοντρόπουλος', 'xontr100@hotmail.com', 'paxontr'),
(4, 'Δέσποινα', 'Λαμόγλου', 'trel0despoinaki@hotm', '1234567'),
(5, 'Στέφανος', 'Μοντελόπουλος', 'stefan@gmail.com', 'stefan69'),
(6, 'Δημήτρης', 'Λαβεγκερίδης', 'lave@outlook.gr', 'tetartesmono'),
(8, 'Oh hi Mark', 'Oh hi Dany', 'bariemai@gmail.com', '666'),
(9, '', '', '', ''),
(11, '', '', 'root', 'support');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `Stars` int(2) NOT NULL,
  `Rating` double DEFAULT NULL,
  `Tel` int(10) NOT NULL,
  `Email` varchar(20) COLLATE utf8_bin NOT NULL,
  `Name` varchar(100) COLLATE utf8_bin NOT NULL,
  `AvailableRooms` int(3) NOT NULL,
  `Area` varchar(100) COLLATE utf8_bin NOT NULL,
  `BankAccount` int(10) NOT NULL,
  `HotelID` int(11) NOT NULL,
  `Address` varchar(100) COLLATE utf8_bin NOT NULL,
  `Num1` int(2) NOT NULL,
  `Num2` int(2) NOT NULL,
  `Num3` int(2) NOT NULL,
  `Num4` int(2) NOT NULL,
  `Num5` int(2) NOT NULL,
  `Price1` double(4,2) NOT NULL,
  `Price2` double(4,2) NOT NULL,
  `Price3` double(4,2) NOT NULL,
  `Price4` double(4,2) NOT NULL,
  `Price5` double(4,2) NOT NULL,
  `Type` varchar(50) COLLATE utf8_bin NOT NULL,
  `Wifi` tinyint(1) DEFAULT NULL,
  `Parking` tinyint(1) DEFAULT NULL,
  `Breakfast` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`Stars`, `Rating`, `Tel`, `Email`, `Name`, `AvailableRooms`, `Area`, `BankAccount`, `HotelID`, `Address`, `Num1`, `Num2`, `Num3`, `Num4`, `Num5`, `Price1`, `Price2`, `Price3`, `Price4`, `Price5`, `Type`, `Wifi`, `Parking`, `Breakfast`) VALUES
(3, 7.8, 2121058749, 'damurhotel@hotmail.c', 'Damur Hotel', 100, 'Ioannina', 2000100082, 2, 'Θησέως 45', 20, 30, 20, 15, 15, 17.00, 39.00, 45.00, 78.00, 99.99, '', 1, 1, 1),
(4, 9.1, 2116587948, 'hoteldeflore@hotmail', 'Hotel De Flore', 70, 'Thessaloniki', 2000215648, 4, 'Ερμού 79', 15, 20, 10, 15, 10, 0.00, 0.00, 0.00, 0.00, 0.00, '', 1, 1, 0),
(3, 6.5, 2132456895, 'sunlightrooms@gmail.', 'Sunlight Appartments\' Hotel', 10, 'Patras', 2007853478, 6, 'Κολοκοτρώνη 4', 0, 0, 2, 6, 2, 0.00, 0.00, 99.99, 99.99, 99.99, '', 1, 1, 0),
(2, 7.5, 2115464895, 'pallashotel@yahoo.co', 'Hotel Pallas', 80, 'Athens', 2100054654, 8, 'Ικάρων 25', 25, 25, 10, 10, 10, 45.00, 60.00, 90.00, 99.99, 99.99, '', 0, 0, 0),
(2, 3.5, 2135189546, 'soulashotel@hotmail.', 'Soula\'s Hotel', 20, 'Athens', 2011546879, 10, 'Στρατιώτου 3', 5, 10, 5, 0, 0, 20.00, 30.00, 50.00, 70.00, 99.99, '', 0, 0, 1),
(4, 8.5, 2115648931, 'Royalpalace@hotmail.', 'Royal Palace', 100, 'Thessaloniki', 2001255465, 12, 'Βηλαρά 69', 25, 0, 20, 15, 10, 0.00, 0.00, 0.00, 0.00, 0.00, '', 1, 1, 0),
(3, 6.1, 2106548952, 'newerahotel@yahoo.co', 'New Era Hotel', 40, 'Thessaloniki', 2100546294, 14, 'Παπανδρέου 15', 15, 15, 10, 0, 0, 10.00, 40.00, 50.00, 0.00, 0.00, '', 1, 0, 1),
(3, 6.3, 2105468792, 'xaraugihotel@yahoo.c', 'Hotel Xaraugi', 30, 'Patras', 2001548953, 16, 'Θεμιστοκλέως 34', 10, 15, 5, 0, 0, 50.00, 60.00, 70.00, 0.00, 0.00, '', 1, 0, 1),
(3, 7.6, 2104568346, 'asteriahotel@hotmail', 'Hotel Asteria', 70, 'Athens', 2000156831, 18, 'Ομήρου 18', 20, 0, 10, 10, 10, 35.00, 45.00, 60.00, 85.00, 99.99, '', 1, 1, 0),
(2, 5.5, 2113546931, 'Jadorehotel@hotmail.', 'J\'adore Hotel', 40, 'Patras', 2000546158, 20, 'Καρϊσκάκη 3', 10, 10, 10, 10, 0, 30.00, 50.00, 70.00, 99.99, 99.99, '', 1, 0, 1),
(5, 0, 2121058733, 'some@gmail.com', 'NewHotel', 123, 'Athens', 2100054666, 21, 'NewAddress 32', 3, 50, 50, 10, 10, 0.00, 0.00, 0.00, 0.00, 0.00, 'blank', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `ReservationID` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `IDFromHotel` int(11) NOT NULL,
  `RoomType` enum('1','2','3','4','5') COLLATE utf8_bin NOT NULL,
  `RoomNum` int(1) NOT NULL,
  `Guests` text COLLATE utf8_bin,
  `GuestsEmail` text COLLATE utf8_bin,
  `FromDate` date NOT NULL,
  `ToDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`ReservationID`, `CustomerId`, `IDFromHotel`, `RoomType`, `RoomNum`, `Guests`, `GuestsEmail`, `FromDate`, `ToDate`) VALUES
(1, 2, 18, '1', 1, 'Κανενας', 'foreveralone', '2019-05-29', '2019-05-31'),
(2, 4, 4, '5', 1, 'Απορρητο', NULL, '2019-06-07', '2019-06-13'),
(3, 6, 20, '2', 3, NULL, NULL, '2019-07-05', '2019-07-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `Myconstraint` (`hotel_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`HotelID`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`ReservationID`),
  ADD KEY `HotelResrv` (`IDFromHotel`),
  ADD KEY `ClientReserv` (`CustomerId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `HotelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `ReservationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `Myconstraint` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`HotelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `ClientReserv` FOREIGN KEY (`CustomerId`) REFERENCES `clients` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `HotelResrv` FOREIGN KEY (`IDFromHotel`) REFERENCES `hotel` (`HotelID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
